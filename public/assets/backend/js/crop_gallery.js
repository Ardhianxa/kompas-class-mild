function master_crop(object,komponen,width,height) 
{
    komponenSubmit = "";
    komponen = JSON.parse(komponen);
    for(submitI=0;submitI<komponen.length;submitI++){
    komponenSubmit += komponen[submitI]+'-';
    }
    komponenSubmit = komponenSubmit.slice(0, -1);
    crop_image(object,komponen,width,height);
    var form = document.getElementById('form-file-manager-data');
    form.onsubmit = function() {
      return checkCoords(komponenSubmit);
    }
}

function crop_image(image,komponen,width,height)
{
    width = JSON.parse(width);
    height = JSON.parse(height);
    document.getElementById("crop_preview").innerHTML += "<input type='hidden' name='komponen' value='"+komponen+"'>";
    for(i=0;i<komponen.length;i++)
    {
        var container = komponen[i];
        var w = width[i];
        var h = height[i];
        document.getElementById("crop_preview").innerHTML += "<div class='image-box col-md-4'><img src='' class='crop crop_"+container+"' id='dp_preview_"+container+"' /></div>";
        document.getElementById("crop_value").innerHTML += '<input type="hidden" id="x_'+container+'" name="x_'+container+'" />';
        document.getElementById("crop_value").innerHTML += '<input type="hidden" id="y_'+container+'" name="y_'+container+'" />';
        document.getElementById("crop_value").innerHTML += '<input type="hidden" id="w_'+container+'" name="w_'+container+'" />';
        document.getElementById("crop_value").innerHTML += '<input type="hidden" id="h_'+container+'" name="h_'+container+'" />';
        document.getElementById("crop_value").innerHTML += '<input type="hidden" id="width_'+container+'" value="'+w+'" name="width_'+container+'" />';
        document.getElementById("crop_value").innerHTML += '<input type="hidden" id="height_'+container+'" value="'+h+'" name="height_'+container+'" />';
    }

    if (image != "") {
        var request = new XMLHttpRequest();
        request.open('GET', "http://local.classmild.id/assets/backend/uploads/original/"+image, true);
        request.responseType = 'blob';
        request.onload = function() {
            var reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload =  function(e){
                for(ii=0;ii<komponen.length;ii++)
                {
                    var container2 = komponen[ii];
                    width = document.getElementById('width_'+container2).getAttribute('value');
                    height = document.getElementById('height_'+container2).getAttribute('value');
                    ratio_w = getRatio(width,height)[0];
                    ratio_h = getRatio(width,height)[1];
                    
                    var text = komponen[ii];
                    crop(text,e,container2,ratio_w.toFixed(2),ratio_h.toFixed(2));
                }
            };
        };
        request.send();
    }
}

function cropGetSegment(text)
{
    var split = text.split("garing");
    return split[(split.length)-1];
}

function crop(text,e,act,ratio_W,ratio_H)
{
    var text = cropGetSegment(text);
    $('#dp_preview_'+act).replaceWith('<div style="color:white;width:100%;background-color:Red;">'+text.toUpperCase()+'</div> <img class="crop crop_'+act+'" id="dp_preview_'+act+'" src="' + e.target.result + '" /><br><br>');
    $('.crop_'+act).Jcrop({
        onSelect: function(c){ 
          responsiveCoords(c,'#dp_preview_'+act,act);
        },
        onChange: function(c){ 
          responsiveCoords(c,'#dp_preview_'+act,act);
        },
        bgOpacity: '.5',
        bgColor: 'black',
        addClass: 'jcrop-dark',
        aspectRatio: parseFloat(ratio_W) / parseFloat(ratio_H),
        allowResize: true
    });
}

function getRatio(width,height)
{
    if(parseInt(width) > parseInt(height))
    {
        w = parseInt(width)/parseInt(height);
        h = 1;
    }
    else if(parseInt(height) > parseInt(width))
    {
        w = 1;
        h = parseInt(height)/parseInt(width);
    }
    else
    {
        w = 1;
        h = 1;
    }

    var result = [w,h];
    return result;
}
 
function responsiveCoords(c, imgSelector, act) {
    var imgOrignalWidth     = $(imgSelector).prop('naturalWidth');
    var imgOriginalHeight   = $(imgSelector).prop('naturalHeight');

    var imgResponsiveWidth  = parseInt($(imgSelector).css('width'));
    var imgResponsiveHeight = parseInt($(imgSelector).css('height'));                

    var responsiveX         = Math.ceil((c.x/imgResponsiveWidth) * imgOrignalWidth);
    var responsiveY         = Math.ceil((c.y/imgResponsiveHeight) * imgOriginalHeight);

    var responsiveW         = Math.ceil((c.w/imgResponsiveWidth) * imgOrignalWidth);
    var responsiveH         = Math.ceil((c.h/imgResponsiveHeight) * imgOriginalHeight);

    $('#x_'+act).val(responsiveX);
    $('#y_'+act).val(responsiveY);
    $('#w_'+act).val(responsiveW);
    $('#h_'+act).val(responsiveH);

    
    console.log('X : '+responsiveX+', Y : '+responsiveY+', W : '+responsiveW+', H :'+responsiveH);
};

function checkCoords(box)
{
    var sukses = 0;
    var i;
    var myBox = box.split("-");

    for(i=0;i<myBox.length;i++)
    {
        var container = myBox[i];
        if(parseInt($('#w_'+container).val())){
            sukses++;
        }
    }
    
    if(sukses==(parseInt(myBox.length)))
    {
        return true;
    }
    Block('Foto harus di crop dahulu.');
    return false;
};

function Block(teks)
{
  sweetAlert("Oops...", teks, "warning");
}