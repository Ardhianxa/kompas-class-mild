<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>owl</title>
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<style type="text/css">
.slidetabs.active {
    color: #ce2d27;
    text-decoration: underline;
}
</style>
</head>

<body>
<div class="owl-carousel owl-theme owl-loaded owl-drag">
  <div class="owl-stage-outer">
    <div class="owl-stage">
      <div class="owl-item">
        <div class="item" data-hash="cowboy_coaster">
          <h1>COWBOY COASTER</h1>
        </div>
      </div>
      <div class="owl-item">
        <div class="item" data-hash="night_tubing">
          <h1>NIGHT TUBING</h1>
        </div>
      </div>
      <div class="owl-item">
        <div class="item" data-hash="nightmare">
          <h1>NIGHTMARE</h1>
        </div>
      </div>
      <!-- . --> 
      <!-- . --> 
      <!-- . --> 
      <!-- . --> 
    </div>
  </div>
</div>
</div>
<a class="night_tubing slidetabs" href="#night_tubing">NIGHT TUBING</a>
<a class="cowboy_coaster slidetabs" href="#cowboy_coaster">COWBOY COASTER</a>
<a class="nightmare slidetabs" href="#nightmare">NIGHTMARE</a>

<script src="js/jquery-3.3.1.min.js"></script> 
<script src="js/owl.carousel.min.js"></script> 
<script type="text/javascript">
jQuery(document).ready(function($) {
  $('.owl-carousel').on('initialized.owl.carousel changed.owl.carousel', function(e) {
    if (!e.namespace)  {
      return;
    }
    var carousel = e.relatedTarget;
    $('.slider-counter').text(carousel.relative(carousel.current()) + 1 + ' / ' + carousel.items().length);
    }).owlCarousel({
      nav:false,
      navText: ["<img class='slider-arrow' src='/wp-content/uploads/2019/01/slider-arrow-pre.png'><div class='slider-counter'>1 / 3</div>","<img class='slider-arrow' src='/wp-content/uploads/2019/01/slider-arrow-next.png'>"],
      loop:true,
      dots:false,
      URLhashListener:true,
      startPosition: '#cowboy_coaster',
      autoplay:false,
      autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true
        }
    }
  });

    $('.owl-carousel').on('changed.owl.carousel', function(event) {
      var current = event.item.index;
      var hash = $(event.target).find(".owl-item").eq(current).find(".item").attr('data-hash');
      $('.'+hash).addClass('active');
    });

    $('.owl-carousel').on('change.owl.carousel', function(event) {
      var current = event.item.index;
      var hash = $(event.target).find(".owl-item").eq(current).find(".item").attr('data-hash');
      $('.'+hash).removeClass('active');
    });

});	
</script>
</body>
</html>