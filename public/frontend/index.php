<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/clipstyle.css" rel="stylesheet">
<link href="css/onepage-scroll.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
</head>
<body>
<div class="headerhome">
  <div class="container-fluid">
    <?php include_once ('mainmenu.php'); ?>
  </div>
</div>
<div class="sidemenu">
  <div class="sidebar">
    <div class="logosec text-center"> <img src="images/Logo-CM01.png"> </div>
    <div class="sidebar-menu">
      <p style="margin-left: 40px;"><a href="#!" id="buka-menu"><img src="images/menu.svg" width="40px"></a><a href="#!" id="tutup-menu"><img src="images/close.svg" width="20px"></a></p>
    </div>
    <div class="slidemenu">
      <ul>
        <li><a href="#!">Ini Asli Gue</a></li>
        <li><a href="#!">Contact Us</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="satuhalaman">
  <section class="isicontent">
    <div class="section fp-auto-height" style="background-color:#4D4D4D">
      <div class="bg_splash">
        <div class="scrolldown"> <object type="image/svg+xml" data="images/arrow_scroll_an.svg" width="130px"></object></div>
        <div class="row m-0 p-0 pwh"> </div>
      </div>
    </div>
  </section>
  <section>
    <div class="clip01">
      <div class="clipthumb01"> </div>
      <div class="menutx01"><a href="campaign-story.php">CAMPAIGN<br>
        STORY </a></div>
    </div>
    <div class="clip02">
      <div class="clipthumb02"> </div>
      <div class="menutx02"><a href="cerita-clasmild.php">CERITA<br>
        CLAS MILD</a></div>
    </div>
    <div class="clip03">
      <div class="clipthumb03"> </div>
      <div class="menutx03"><a href="produk.php">TENTANG<br>
        PRODUK</a></div>
    </div>
    <div class="clip04">
      <div class="clipthumb04"> </div>
      <div class="menutx04"><a href="suara-konsumen.php">SUARA<br>
        KONSUMEN</a></div>
    </div>
    <div class="pwh-02">
      <div class="copyright"> copyright &copy; 2020 Clas Mild. All Right Reserved <br>
        <a href="#!">Privacy Policy</a> | <a href="#!">Term of Use</a> | <a href="#!">Legal</a> </div>
      <div class="row peringatan" >
        <div class="col-sm"><img src="images/neck_11.jpg" alt="" width="150px"></div>
        <div class="col-sm text-center">PERINGANTAN : <br>
          KARENA MEROKOK SAYA TERKENA KANKER TENGGOROKAN. LAYANAN BERHENTI MEROKOK (0800-0177-65661)</div>
        <div class="col-sm text-right"><img src="images/18.png" alt=""></div>
      </div>
    </div>
  </section>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery-3.3.1.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap-4.3.1.js"></script> 
<script type="text/javascript" src="js/jquery.onepage-scroll.js"></script> 
<script type="text/javascript">
	  $(document).ready(function(){
      $(".satuhalaman").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: 600,
        loop: true
      });
		});
		$(document).ready(function(){
  $("#buka-menu").click(function(){
    $(".slidemenu").addClass("openslidemenu");
	  $("#tutup-menu").css({"display": "inline-block"});
	  $("#buka-menu").css({"display": "none"});
  });
  $("#tutup-menu").click(function(){
    $(".slidemenu").removeClass("openslidemenu");
	  $("#buka-menu").css({"display": "inline-block"});
	  $("#tutup-menu").css({"display": "none"});
  });
});
	</script>
</body>
</html>