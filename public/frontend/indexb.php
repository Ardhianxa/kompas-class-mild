<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>

<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<section class="headerhome">
  <div class="container-fluid">
    <?php include_once ('mainmenu.php'); ?>
  </div>
</section>
<section class="isicontent">
  <div class="row">
    <div class="col-sm-2 p-0 m-0">
      <div class="sidebar">
        <div class="logosec text-center"> <img src="images/Logo-CM01.png"> </div>
        <div class="sidebar-menu">
          <ul>
            <li><a href="#!">INI ASLI GUE</a></li>
            <li><a href="#!">CONTACT US</a></li>
          </ul>
        </div>
        <div class="sidebar-sosmed"> <a href="#!"><img src="images/sosmed_01.png"></a> <a href="#!"><img src="images/sosmed_03.png"></a> <a href="#!"><img src="images/sosmed_05.png"></a> <a href="#!"><img src="images/sosmed_07.png"></a> </div>
      </div>
    </div>
    <div class="col-sm-10 p-0 m-0">
      <div class="container-fluid rightbar">
        <div class="clip01">
          <div class="clipthumb01"> </div>
          <div class="menutx01"> CAMPAIGN<br>
            STORY </div>
        </div>
        <div class="clip02">
          <div class="clipthumb02"> </div>
          <div class="menutx02">CERITA<br>
            CLAS MILD</div>
        </div>
        <div class="clip03">
          <div class="clipthumb03"> </div>
          <div class="menutx03">TENTANG<br>
            PRODUK</div>
        </div>
        <div class="clip04">
          <div class="clipthumb04"> </div>
          <div class="menutx04">SUARA<br>
            KONSUMEN</div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<?php include ('javaskrip.php') ?>
</body>
</html>
