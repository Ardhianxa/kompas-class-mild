<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Class Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
	<link href="css/modal-video.min.css" rel="stylesheet">
	<style type="text/css">
		.story {
			color:#fff !important;
		}
	</style>
</head>
<body>
<div class="container-fluid">
 <?php include_once ('mainmenu.php'); ?>
</div>
<section>
  <div id="demo" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false"> 
    <!-- Indicators -->
    <div class="carousel-indicators tahuntx">
		<span><a href="#!" data-target="#demo" data-slide-to="0" class="active">
			<font>2003 - 2004</font>
			<font>2005</font>
			<font>2006 - 2007</font>
			</a></span>
		<span><a href="#!" data-target="#demo" data-slide-to="1">
			<font>2008 - 2012</font>
			<font>2013 - 2015</font>
			<font>2016</font>
			</a></span>
		<span><a href="#!" data-target="#demo" data-slide-to="2">
			<font>2019</font>
			<font>2020</font>
			<font>2021</font>
			</a></span>
	  </div>
    
    <!-- The slideshow -->
    <div class="carousel-inner h-100">
		
      <div class="carousel-item active">
        <div class="slider_container bgslide-01">
          <div class="container">
            <div class="row articlebox">
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb">
						<a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/videoplayback.mp4"><img src="images/2003.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/YIG-RaveNew.mp4"><img src="images/2005.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//dc694.4shared.com/img/ydQ36_JCea/f62b723f/dlink__2Fdownload_2FydQ36_5FJCea_3Fsbsr_3Db03cbc0cd1552d2e7f727011e1bd8756a63_26bip_3DMTE4LjEzNy4yMTcuMTQy_26lgfp_3D66_26dsid_3D-GvWfA7l.cbced19702e0d6746cb6c8de903e844e_26bip_3DMTE4LjEzNy4yMTcuMTQy_26bip_3DMTE4LjEzNy4yMTcuMTQy/preview.mp4?cuid=1443571904&cupa=7209ada200ed2eb1c96042b021c9de2c"><img src="images/2006.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
		
		<div class="carousel-item">
        <div class="slider_container bgslide-02">
          <div class="container">
            <div class="row articlebox">
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb">
						<a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/TLDM-RockConcertNew.mp4"><img src="images/2008.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/09/AN-SenyumNew.mp4"><img src="images/2013.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clasmild_30Rev27092016....mp4"><img src="images/2016.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
		
		<div class="carousel-item">
        <div class="slider_container bgslide-03">
          <div class="container">
            <div class="row articlebox">
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb">
						<a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2018/08/CLASSMILD-6-15-Dtk-Versi-Kerang.mp4"><img src="images/2019.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clas-Mild_The-Journey_30s.mp4"><img src="images/2020.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clas-Mild_The-Shadow-Painter_30s_170224....mp4"><img src="images/2021.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
		
      
		
		
		
    </div>
  </div>
</section>
	<!-- MODAL -->
	<div class="modal fade" id="modal01" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<button type="button" class="close text-right" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
        <video width="320" height="240" controls>
  <source src="https://clas-mild.com/wp-content/uploads/2017/08/videoplayback.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
      </div>
    </div>
  </div>
</div>
<section>
  <?php include_once ('footers.php');?>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery-3.3.1.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap-4.3.1.js"></script>
<script src="js/jquery-modal-video.min.js"></script>
	<script>
		$(".js-video-button").modalVideo({
			youtube:{
				controls:1,
				nocookie: true
			}
		});
	</script>
</body>
</html>
