<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/modal-video.min.css" rel="stylesheet">
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<style type="text/css">
.story {
    color: #fff !important;
}
.tahunxx.active {
    color: #ce2d27 !important;
    text-decoration: underline !important;
}
.bgslide-01 {
    background: url('images/bg-slider01.jpg') top center no-repeat;
    background-size: cover;
}
</style>
</head>
<body style="background-color:#888;">
<div class="container-fluid">
  <?php include_once ('mainmenu.php'); ?>
</div>
<section class="bgslide-01">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="owl-carousel owl-theme owl-loaded">
          <div class="item" data-hash="01" style="width:100%;">
            <div class="vid-thumb"> <a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/videoplayback.mp4"><img src="images/2003.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="02">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/YIG-RaveNew.mp4"><img src="images/2005.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="03">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//dc694.4shared.com/img/ydQ36_JCea/f62b723f/dlink__2Fdownload_2FydQ36_5FJCea_3Fsbsr_3Db03cbc0cd1552d2e7f727011e1bd8756a63_26bip_3DMTE4LjEzNy4yMTcuMTQy_26lgfp_3D66_26dsid_3D-GvWfA7l.cbced19702e0d6746cb6c8de903e844e_26bip_3DMTE4LjEzNy4yMTcuMTQy_26bip_3DMTE4LjEzNy4yMTcuMTQy/preview.mp4?cuid=1443571904&cupa=7209ada200ed2eb1c96042b021c9de2c"><img src="images/2006.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="04">
            <div class="vid-thumb"> <a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/TLDM-RockConcertNew.mp4"><img src="images/2008.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="05">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/09/AN-SenyumNew.mp4"><img src="images/2013.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="06">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clasmild_30Rev27092016....mp4"><img src="images/2016.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="07">
            <div class="vid-thumb"> <a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2018/08/CLASSMILD-6-15-Dtk-Versi-Kerang.mp4"><img src="images/2019.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="08">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clas-Mild_The-Journey_30s.mp4"><img src="images/2020.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
			<div class="item" data-hash="09">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clas-Mild_The-Journey_30s.mp4"><img src="images/2020.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tahuntx"> 
	  <a href="#01" id="thn" class="active thn01">
		  <span>2003 - 2004</span>&nbsp;
		  <span>2005</span>&nbsp;
		  <span>2006-2007</span>&nbsp;
	  </a>
	  <a href="#04" id="thn" class="thn02">
		  <span>2008 -2012</span>&nbsp;
		  <span>2013-2015</span>&nbsp;
		  <span>2016</span>&nbsp;
	  </a>
	  <a href="#07" id="thn" class="thn03">
		  <span>2019</span>&nbsp;
		  <span>2020</span>&nbsp;
		  <span>2021</span>&nbsp;
	  </a>
	</div>
</section>
<!-- end content section -->

<section>
  <?php include_once ('footers.php');?>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- JS --> 
<script src="js/jquery-3.3.1.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap-4.3.1.js"></script> 
<script src="js/jquery-modal-video.min.js"></script> 
<script src="js/owl.carousel.min.js"></script> 
<script>
		$(".js-video-button").modalVideo({
			youtube:{
				controls:1,
				nocookie: true
			}
		});
	</script> 
<script type="text/javascript">
jQuery(document).ready(function() {
	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
	URLhashListener: true,
    responsive:{
        0:{
            items:1,
			nav: false,
			loop:true
        },
        600:{
            items:3
        },
        1000:{
            items:3,
			dots: false,
			nav: false,
			touchDrag: false
        }
    }
})
           
            });
	$(document).on('click', '#thn', function() {
		$(this).addClass('active').siblings().removeClass('active')
	})
	
	$(document).ready(function(){
		$('.thn01').click(function() {
			$('.bgslide-01').css({
				'background': "url('images/bg-slider01.jpg') top center no-repeat",
				'background-size': "cover",
			});
		});	
		$('.thn02').click(function() {
			$('.bgslide-01').css({
				'background': "url('images/bg-slider02.jpg') top center no-repeat",
				'background-size': "cover",
			});
		});	
		$('.thn03').click(function() {
			$('.bgslide-01').css({
				'background': "url('images/bg-slider03.jpg') top center no-repeat",
				'background-size': "cover",
			});
		});	
	});
</script>
</body>
</html>
