<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/clipstyle.css" rel="stylesheet">
</head>

<body>
<section class="headerhome">
  <div class="container-fluid">
    <?php include_once ('mainmenu.php'); ?>
  </div>
</section>
<section class="isicontent">
  <div class="row">
    <div class="col-sm-2 p-0 m-0">
      <div class="sidebar" id="scrollbox">
        <div class="logosec text-center"> <img src="images/Logo-CM01.png"> </div>
        <div class="sidebar-menu">
          <ul>
            <li><a href="#!">INI ASLI GUE</a></li>
            <li><a href="#!">CONTACT US</a></li>
          </ul>
        </div>
        <div class="sidebar-sosmed" >
          <div class="row">
            <div class="col-sm m-0 p-0 d-none"><a href="#!"><img src="images/sosmed_01.png" class="img-fluid"></a></div>
            <div class="col-sm m-0 p-0"><a href="#!"><img src="images/sosmed_03.png" class="img-fluid"></a></div>
            <div class="col-sm m-0 p-0 d-none"><a href="#!"><img src="images/sosmed_05.png" class="img-fluid"></a></div>
            <div class="col-sm m-0 p-0"><a href="#!"><img src="images/sosmed_07.png" class="img-fluid"></a></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-10 p-0 m-0">
      <div class="container-fluid rightbar"> 
        <!-- START ONE PAGE -->
        <div id="fullpage">
          <div class="section " id="section0" style="background: #000">
            <div class="slide">
              <h1>One single section</h1>
            </div>
            <div class="slide">
              <h1>And a normal website under it</h1>
            </div>
            <div class="slide">
              <h1>A great single slider!</h1>
            </div>
          </div>
        </div>
        <!-- END ONE PAGE --> 
      </div>
    </div>
  </div>
</section>
<section>
  <div class="row m-0 p-0">
    <div class="col-sm m-0 p-0">
      <div class="copyright"> copyright &copy; 2020 Clas Mild. All Right Reserved | <a href="#!">Privacy Policy</a> | <a href="#!">Term of Use</a> | <a href="#!">Legal</a> </div>
      <div class="row peringatan" >
        <div class="col-sm"><img src="images/neck_11.jpg"></div>
        <div class="col-sm text-center">PERINGANTAN : <br>
          KARENA MEROKOK SAYA TERKENA KANKER TENGGOROKAN. LAYANAN BERHENTI MEROKOK (0800-0177-65661)</div>
        <div class="col-sm text-right"><img src="images/18.png"></div>
      </div>
    </div>
  </div>
</section>
<?php include ('javaskrip.php') ?>
<script type="text/javascript">
   $(document).ready(function() {
	$('#fullpage').fullpage({
		autoScrolling: false,
		fitToSection: false
	});
});
    });
</script> 
</script>
</body>
</html>