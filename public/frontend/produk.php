<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
	<style type="text/css">
		.produk {
			color:#fff !important;
		}
	</style>
</head>
<body>
	<!--<div class="testimg">
		<img src="images/CM16.jpg" alt="">
	</div>-->
<div class="container-fluid">
  <?php include_once ('mainmenu.php'); ?>
</div>
<section class="bg_produk">
  <div class="container-fluid">
    <div class="container text-center">
      <div class="row">
        <div class="col-sm">
          <h2 class="txhidden">PRODUK KAMI</h2>
          <p class="m-5 txhidden">Terbuat dari bahan tembakau terbaik, Clas Mild mengeluarkan 3 varian SKU Clas Mild 12, Clas Mild Silver dan Clas Mild 16, untuk konsumen yang mengutamakan kualitas dan cita rasa otentik.</p>
        </div>
      </div>
    </div>
    <div class="container" id="produk-rokok">
      <div class="row">
        <div class="col-sm" id="cm12box">
          <div class="flexbox">
            <div class="fleximg"><img src="images/CM12.png" alt="" class="img-fluid">
              <div class="plus"> <a href="#!" id="plus01"><img src="images/plus.png" alt=""></a> </div>
              <img src="images/close.png" alt="" class="tutup01"> </div>
            <div class="flxtxt">
              <h4>CLAS MILD 12</h4>
              <p class="desc-rokok">aliquip ex ea commodo consequat.<br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br>
                cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
        </div>
		  <div class="col-sm" id="cm16box">
          <div class="flexbox">
            <div class="fleximg"> <img src="images/CM16.png" alt="" class="img-fluid">
              <div class="plus"> <a href="#!" id="plus03"><img src="images/plus.png" alt=""></a> </div>
              <img src="images/close.png" alt="" class="tutup03"> </div>
            <div class="flxtxt">
              <h4>CLAS MILD 16</h4>
              <p class="desc-rokok">aliquip ex ea commodo consequat.<br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br>
                cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
        </div>
        <div class="col-sm" id="cmsbox">
          <div class="flexbox">
            <div class="fleximg"> <img src="images/CMS.png" alt="" class="img-fluid">
              <div class="plus"> <a href="#!" id="plus02"><img src="images/plus.png" alt=""></a> </div>
              <img src="images/close.png" alt="" class="tutup02"> </div>
            <div class="flxtxt">
              <h4>CLAS MILD SILVER</h4>
              <p class="desc-rokok">aliquip ex ea commodo consequat.<br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br>
                cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<?php include ('javaskrip.php') ?>
<script type="text/javascript">
		$(document).ready(function() {
			$("#plus01").click(function() {
				$("#cm12box").addClass("upscale"),
				$(".desc-rokok").addClass("showup"),
				$(".tutup01").addClass("ada"),
				$("#plus01").addClass("hilang"),
				$("#cmsbox").addClass("playfadeout"),
				$("#cm16box").addClass("playfadeout"),
				$(".flexbox").addClass("flexrow"),
				$(".txhidden").addClass("hilang");
			});
		});
		
		$(document).ready(function() {
			$(".tutup01").click(function() {
				$("#cm12box").removeClass("upscale"),
				$(".desc-rokok").removeClass("showup"),
				$(".tutup01").removeClass("ada"),
				$("#plus01").removeClass("hilang"),
				$("#cmsbox").removeClass("playfadeout"),
				$("#cm16box").removeClass("playfadeout"),
				$(".flexbox").removeClass("flexrow"),
				$(".txhidden").removeClass("hilang");
			});
		});
	
	$(document).ready(function() {
			$("#plus02").click(function() {
				$("#cmsbox").addClass("upscale");
				$(".desc-rokok").addClass("showup");
				$(".flexbox").addClass("flexrow");
				$("#plus02").addClass("hilang");
				$(".tutup02").addClass("ada");
				$("#cm12box").addClass("playfadeout");
				$("#cm16box").addClass("playfadeout");
				$(".txhidden").addClass("hilang");
				
			});
		});
	
	$(document).ready(function() {
			$(".tutup02").click(function() {
				$("#cmsbox").removeClass("upscale"),
				$(".desc-rokok").removeClass("showup"),
				$(".tutup02").removeClass("ada"),
				$("#plus02").removeClass("hilang"),
				$("#cm12box").removeClass("playfadeout"),
				$("#cm16box").removeClass("playfadeout"),
				$(".flexbox").removeClass("flexrow"),
				$(".txhidden").removeClass("hilang");
			});
		});
	
	$(document).ready(function() {
			$("#plus03").click(function() {
				$("#cm16box").addClass("upscale");
				$(".desc-rokok").addClass("showup");
				$(".flexbox").addClass("flexrow");
				$("#plus03").addClass("hilang");
				$(".tutup03").addClass("ada");
				$("#cm12box").addClass("playfadeout");
				$("#cmsbox").addClass("playfadeout");
				$(".txhidden").addClass("hilang");
				
			});
		});
	
	$(document).ready(function() {
			$(".tutup03").click(function() {
				$("#cm16box").removeClass("upscale"),
				$(".desc-rokok").removeClass("showup"),
				$(".tutup03").removeClass("ada"),
				$("#plus03").removeClass("hilang"),
				$("#cm12box").removeClass("playfadeout"),
				$("#cmsbox").removeClass("playfadeout"),
				$(".flexbox").removeClass("flexrow"),
				$(".txhidden").removeClass("hilang");
			});
		});
	
	
	</script>
</body>
</html>
