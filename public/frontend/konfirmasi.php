<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid"> </div>
<section class="bg_konfirmasi">
<div class="container-fluid">
<div class="container">
  <div class="row konfirmasibox mb-2">
    <div class="col-sm txshadow text-center">
      <p>
      <h3>INFORMASI DALAM WEBSITE INI DITUJUKAN UNTUK PEROKOK BERUSIA 18 TAHUN ATAU LEBIH DAN TINGGAL DI WILAYAH INDONESIA</h3>
      </p>
      <p> Isi Tanggal Lahir Dulu Ya </p>
    </div>
  </div>
  <div class="container">
	  <div class="d-flex justify-content-center form-group">
	  <div class="m1_input">
          <input type="text" class="form-control" placeholder="DD" aria-label="DD" maxlength="2" style="width: 55px;">
        </div>
		  <div class="m1_input">
          <input type="text" class="form-control" placeholder="MM" aria-label="MM" maxlength="2" style="width: 55px;">
        </div>
		   <div class="m1_input">
          <input type="text" class="form-control" placeholder="YYYY" aria-label="YYYY" maxlength="4" style="width: 100px;">
		  </div>
	  </div>
    <div class="row justify-content-md-center konfirmasibox text-center mb-2">
      <div class="col-md-auto">
        <p class="txshadow">Apakah Anda Merokok?</p>
        <div class="row">
          <div class="col-md-auto">
            <div class="custom-control custom-radio">
              <input type="radio" class="custom-control-input" id="rad01" name="example1" value="customEx">
              <label class="custom-control-label" for="rad01">YA</label>
            </div>
          </div>
          <div class="col-md-auto">
            <div class="custom-control custom-radio">
              <input type="radio" class="custom-control-input" id="rad02" name="example1" value="customEx">
              <label class="custom-control-label" for="rad02">TIDAK</label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-md-center konfirmasibox text-center mb-2">
      <div class="col-md-auto">
        <p class="txshadow">Merek Rokok Yang Dipakai?</p>
        <div class="w-100"></div>
        <div class="col-md-auto">
          <div class="form-group">
            <select class="form-control form-control-lg" id="sel1">
              <option>Clas Mild 16</option>
              <option>Clas Mild 12</option>
              <option>Clas Mild Silver</option>
              <option>A Mild</option>
              <option>Dunhill Mild</option>
              <option>Dunhill Filter</option>
              <option>Magnum Mild</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm text-center">
        <button type="button" class="btn btn-primary btn-lg">KONFIRMASI</button>
      </div>
    </div>
  </div>
</div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<?php include ('javaskrip.php') ?>
</body>
</html>
