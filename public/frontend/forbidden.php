<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid"> </div>
<section class="bg_konfirmasi">
  <div class="container-fluid">
    <div class="container">
      <div class="row forbiddenbox mb-2">
        <div class="col-sm txshadow text-center">
			<p style="margin-bottom: 50px;"><img src="images/Logo-CM01.png" alt="" class="img-fluid"></p>
          <p>
          <h1>MOHON MAAF!<br> ANDA BELUM CUKUP UMUR UNTUK MEMASUKI WEBSITE INI.</h1>
          </p>
		  <p class="p-5"><a href="konfirmasi.php" class="btn btn-primary btn-lg">KEMBALI</a></p>
        </div>
      </div>
  </div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<?php include ('javaskrip.php') ?>
</body>
</html>
