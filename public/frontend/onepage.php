<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>onepage</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/fullpage.css" />
</head>

<body>
<div id="fullpage">
  <div class="section" style="background-color:#4D4D4D">
    <h1>page 1</h1>
  </div>
  <div class="section" style="background-color:#d4d4d4">
    <h1>page 2</h1>
  </div>
</div>
<?php include ('javaskrip.php') ?>
<script type="text/javascript">
	var myFullpage = new fullpage('#fullpage', {
		responsiveHeight: 641,
		normalScrollElements: '#scrollbox',
        afterResponsive: function(isResponsive){
        }
	});
</script>
</body>
</html>
