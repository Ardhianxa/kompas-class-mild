<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
	<style type="text/css">
		.suara {
			color:#fff !important;
		}
	</style>
</head>
<body>
<div class="container-fluid">
  <?php include_once ('mainmenu.php'); ?>
</div>
<section class="suarabox">
  <div class="container text-center">
    <div class="row">
      <div class="col">
        <h2>TRACKING ID</h2>
      </div>
      <div class="row mt-5">
        <div class="col">
          <p>
          <h4>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="container mt-5">
    <div class="row">
      <div class="col">
        <h3 class="text-center">Masukkan ID Pesan Anda :</h3>
        <p>
        <div class="form-group">
          <label for="tx01">INPUT ID</label>
          <input type="email" class="form-control" id="tx01" aria-describedby="emailHelp" placeholder="">
          <small id="emailHelp" class="form-text text-muted">*Pesan ID Dikirim VIA EMail Anda Yang Sudah Terdaftar</small> </div>
        </p>
        <p>
        <h4>GOOGLE RECAPTHA HERE</h4>
        </p>
        <p>
          <button type="button" class="btn btn-primary btn-lg">CARI</button>
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col bg-light mb-5">
        <table class="table table-striped table-responsive-sm">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">NAMA</th>
              <th scope="col">JENIS PERTANYAAN</th>
              <th scope="col">JUDUL PESAN</th>
				<th scope="col">EMAIL</th>
				<th scope="col">STATUS</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">id-3994019</th>
              <td>Irwan Dian Kusuma</td>
              <td>Pertanyaan</td>
              <td>Rasa Class Mild</td>
				<td>djaja@yahoo.com</td>
				<td>Diproses</td>
            </tr>
			  <tr>
              <th scope="row">id-4809368</th>
              <td>Wahyu Ridwan Tanuwidjaja</td>
              <td>Pertanyaan</td>
              <td>Warna Class Mild</td>
				<td>wahyu@yahoo.com</td>
				<td>Diproses</td>
            </tr>
			  <tr>
              <th scope="row">id-6662378</th>
              <td>Darma Dwi Kusnadi</td>
              <td>Pertanyaan</td>
              <td>Ukuran Class Mild</td>
				<td>darma@yahoo.com</td>
				<td>Diproses</td>
            </tr>
			  <tr>
              <th scope="row">id-9058956</th>
              <td>Eka Dwi Widjaja</td>
              <td>Pertanyaan</td>
              <td>Beli Class Mild</td>
				<td>egadwi@yahoo.com</td>
				<td>Diproses</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<?php include ('javaskrip.php') ?>
</body>
</html>
