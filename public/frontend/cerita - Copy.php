<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Class Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
 <?php include_once ('mainmenu.php'); ?>
</div>
<section>
  <div id="demo" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false"> 
    <!-- Indicators -->
    <div class="carousel-indicators tahuntx"> <span><a href="#!" data-target="#demo" data-slide-to="0" class="active">2003</a></span> <span><a href="#!" data-target="#demo" data-slide-to="1">2002</a></span> <span><a href="#!" data-target="#demo" data-slide-to="2">2003</a></span> <span><a href="#!" data-target="#demo" data-slide-to="3">2004</a></span> </div>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="slider_container bgslide-01">
          <div class="container">
            <div class="row articlebox">
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="slider_container bgslide-02">
          <div class="container">
            <div class="row articlebox" >
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="slider_container bgslide-03">
          <div class="container">
            <div class="row articlebox" >
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="slider_container bgslide-04">
          <div class="container">
            <div class="row articlebox" >
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
              <div class="col-sm">
                <div class="row">
                  <div class="col">
                    <div class="vid-thumb"> <a href="detail-cerita.php"><img src="images/vid.jpg" class="img-fluid"></a> </div>
                    <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery-3.3.1.min.js"></script> 

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap-4.3.1.js"></script>
</body>
</html>
