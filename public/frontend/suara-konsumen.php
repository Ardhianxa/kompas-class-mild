<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Clas Mild</title>
<!-- Bootstrap -->
<link href="css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
	<style type="text/css">
		.suara {
			color:#fff !important;
		}
	</style>
</head>
<body>
<div class="container-fluid">
  <?php include_once ('mainmenu.php'); ?>
</div>
<section class="suarabox">
  <div class="container text-center">
    <div class="row">
      <div class="col">
        <h2>SUARA KONSUMEN</h2>
      </div>
      <div class="row mt-5">
        <div class="col">
          <p>
          <h4 class="p-2">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</h4>
          <p class="mt-5"><a href="tracking-id.php" class="btn btn-outline-info">CHECK TRACKING ANDA DISINI</a></p>
          </p>
          <p class="mt-5">
          <h3 class="text-center">Masukkan Data Anda*</h3>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="form-group">
          <label for="tx01">Nama</label>
          <input type="text" class="form-control" id="tx01">
        </div>
        <div class="form-group">
          <label for="tx02">email</label>
          <input type="text" class="form-control" id="tx02">
        </div>
        <div class="form-group">
          <label for="tx05">kode pos</label>
          <input type="text" class="form-control" id="tx05">
        </div>
        <div class="form-group">
          <label for="tx06">Alamat Lengkap</label>
          <textarea class="form-control" id="tx06" rows="3"></textarea>
        </div>
        <div class="form-group">
          <label for="tx07">No. smartphone (Whatsapp aktif)</label>
          <input type="text" class="form-control" id="tx07">
        </div>
        <div class="separates-border"> </div>
        <div class="form-group">
          <label for="tx08">tujuan pernyataan*</label>
          <select class="form-control" id="tx08">
            <option>Apresiasi</option>
            <option>Komplain</option>
            <option>Pertanyaan</option>
          </select>
        </div>
        <div class="form-group">
          <label for="tx08">Silahkan Pilih Pesan*</label>
          <select class="form-control" id="tx08">
            <option>Periklanan</option>
            <option>Ketersediaan Produk</option>
            <option>Kualitas Produk</option>
          </select>
        </div>
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <label for="tx09">nama produk</label>
              <select class="form-control form-control-lg" id="sel1" id=tx09>
                  <option>Clas Mild 16</option>
                  <option>Clas Mild 12</option>
                  <option>Clas Mild Silver</option>
                </select>
            </div>
			  <div class="form-group">
                
              </div>
            <div class="form-group">
              <label for="tx10">tanggal produksi</label>
              <input type="date" class="form-control" id="tx10">
            </div>
            <div class="form-group">
              <label for="tx11">Kode produksi</label>
              <input type="text" class="form-control" id="tx11">
            </div>
            <div class="form-group">
              <label for="tx11">Lokasi Pembelian</label>
              <div id="mapbox">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63453.51894859086!2d106.76706004128987!3d-6.284105385612779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1ec2422b0b3%3A0x39a0d0fe47404d02!2sJakarta%20Selatan%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta!5e0!3m2!1sid!2sid!4v1604678151232!5m2!1sid!2sid" width="100%" height="auto" frameborder="0" style="border:0; min-height: 450px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <button type="button" class="btn btn-secondary">Deteksi Lokasi Saya</button>
              </div>
              <p></p>
              <div class="form-group">
                <textarea class="form-control" id="tx12" rows="3" placeholder="Alamat Lengkap Lokasi Pembelian"></textarea>
              </div>
            </div>
          </div>
        </div>
        <p></p>
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <label for="tx12">nama produk</label>
              <input type="text" class="form-control" id="tx12">
            </div>
            <div class="form-group">
              <label for="tx11">Lokasi Pembelian</label>
              <div id="mapbox">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63453.51894859086!2d106.76706004128987!3d-6.284105385612779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1ec2422b0b3%3A0x39a0d0fe47404d02!2sJakarta%20Selatan%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta!5e0!3m2!1sid!2sid!4v1604678151232!5m2!1sid!2sid" width="100%" height="auto" frameborder="0" style="border:0; min-height: 450px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <button type="button" class="btn btn-secondary">Deteksi Lokasi Saya</button>
              </div>
              <p></p>
              <div class="form-group">
                <textarea class="form-control" id="tx13" rows="3" placeholder="Alamat Lengkap Lokasi Pembelian"></textarea>
              </div>
            </div>
          </div>
        </div>
        <p></p>
        <div class="form-group">
          <label for="tx14">Pesan Anda</label>
          <textarea class="form-control" id="tx14" rows="4"></textarea>
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend"> <span class="input-group-text">Upload</span> </div>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="tx15">
            <label class="custom-file-label" for="tx15">Pilih file upload. (*ukuran max. 5MB Format : .jpg/.png/.pdf/.zip/.word/.ppt)</label>
          </div>
        </div>
		  <div class="form-group">
			  	<h3>Google recaptha here</h3>
		  </div>
		  <p><button type="button" class="btn btn-primary btn-lg">KIRIM</button></p>
      </div>
    </div>
  </div>
</section>
<section>
  <?php include_once ('footers.php');?>
</section>
<?php include ('javaskrip.php') ?>
</body>
</html>
