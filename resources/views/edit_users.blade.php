@extends('layouts.app')  

@section('content')
<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card">
        <form method="post" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('users-edit-post', ['status' => $status, 'id' => $user->id]) }}">
          @csrf
          <div class="card-body">
              	<div class="row">                               
                  <div class="form-group col-md-12 col-12">
                    	<label>Email</label>
                    	<input type="text" class="form-control" name="email" value="{{ $user->email }}" required="" readonly>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                  </div>
              	</div>
              	<div class="row">
              		@foreach($role as $value)
                  <div align="center" class="form-group col-md-2 col-sm-2 col-lg-2" style="margin-right:3px;">
                      <label style="font-size:15px;">{{ $value->name }}</label>
                      @php
                        $checked = '';
                        if($user->hasRole($value->name)){
                          $checked = "checked='checked'";
                        }
                      @endphp
                    	<div style="margin-top:2px;">
                      	<input type="checkbox" {{ $checked }} name="privileges[]" class="form-control" id="customCheck{{ $value->id }}" value="{{ $value->name }}">
                    </div>
                  </div>
                  @endforeach
              	</div>
          </div>
          <div class="card-footer text-right">
            <button class="btn btn-primary">Submit</button>
          </div>
        </form>
    </div>
</div>
@endsection