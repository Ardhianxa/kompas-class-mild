@extends('layouts.app') 

@section('content')
<section class="section">
    <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              <img src="{{ config('constant.ASSETS_URL').'backend/img/clasmild.jpg' }}" alt="logo" width="200" class="shadow-light rounded-circle">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Activation Account</h4></div>

              <div class="card-body">
                <form method="POST" action="{{ route('users-activation',['token' => $token]) }}" class="needs-validation" novalidate="">
                    @csrf
                  <div class="form-group">
                    <label for="new-password">Email</label>
                    <input id="new-password" type="text" class="form-control" readonly="readonly" name="email" value="{{ $email }}">
                    <div class="invalid-feedback">
                      Please fill in your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new-password">First Name</label>
                    <input id="new-password" type="text" class="form-control" required name="first_name">
                    <div class="invalid-feedback">
                      Please fill in your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new-password">Last Name</label>
                    <input id="new-password" type="text" class="form-control" required name="last_name">
                    <div class="invalid-feedback">
                      Please fill in your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new-password">Phone Number</label>
                    <input id="new-password" type="text" class="form-control" required name="handphone">
                    <div class="invalid-feedback">
                      Please fill in your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new-password">Password</label>
                    <input id="new-password" type="password" class="form-control" name="password" required autocomplete="new-password"><br>
                    * Minimal 6 Karakter
                    <div class="invalid-feedback">
                      Please fill in your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password-confirm">Retype Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="password-confirm"><br>
                    * Minimal 6 Karakter
                    <div class="invalid-feedback">
                      Please retype your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Submit
                    </button>
                  </div>
                  <input type="hidden" name="token" value="{{ $token }}">
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection

