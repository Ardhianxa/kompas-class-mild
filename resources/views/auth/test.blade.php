<!DOCTYPE html>
<html lang="en">
<head>
  @include('global.metadata')
  @include('global.assets')
</head>
<body class="hold-transition login-page">
    <div id="app">
        <app></app>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
@include('global.assets_footer')
</html>