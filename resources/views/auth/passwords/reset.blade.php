@extends('layouts.app')  

@section('content')
<section class="section">
    <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              <img src="{{ config('constant.ASSETS_URL').'backend/img/clasmild.jpg' }}" alt="logo" width="200" class="shadow-light rounded-circle">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Create New Password</h4></div>

              <div class="card-body">
                <form method="POST" action="{{ route('password.update') }}" class="needs-validation" novalidate="">
                    @csrf
                  <div class="form-group">
                    <label for="new-password">New Password</label>
                    <input id="new-password" type="password" class="form-control" name="password" required autocomplete="new-password">
                    <div class="invalid-feedback">
                      Please fill in your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password-confirm">Retype New Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="password-confirm">
                    <div class="invalid-feedback">
                      Please retype your new password
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Reset
                    </button>
                  </div>
                  <input type="hidden" name="token" value="{{ $token }}">
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection

