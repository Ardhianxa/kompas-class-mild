<div class="col-12 col-md-12 col-lg-6">
    <select id="order-image" class="form-control" onchange="file_manager_get_data('{{config('constant.APP_URL')}}','image',1,'{{json_encode($config)}}')">
        <option value="desc" @if($order == 'desc') selected='selected' @endif>Latest</option>
        <option value="asc"  @if($order == 'asc') selected='selected' @endif>Oldest</option>
    </select>
</div>
<div class="col-12 col-md-12 col-lg-6">
    <div class="input-group mb-3">
        <input type="text" class="form-control" id="search-image" placeholder="Search" aria-label="" value="{{$key}}">
        <div class="input-group-append">
            <a onclick="file_manager_get_data('{{config('constant.APP_URL')}}','image',1,'{{json_encode($config)}}')" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
        </div>
    </div>
</div>