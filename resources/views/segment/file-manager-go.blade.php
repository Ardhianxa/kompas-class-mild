@if(!$editor)
    @if(isset($crop->status) && $crop->status)
        <input type="hidden" name="crop" value="true">
        <input type="hidden" id="{{$container_image}}" name="image_data" value="{{$filename}}">
        <input type="hidden" id="komponen" name="komponen" value="{{json_encode($komponen)}}">
        <input type="hidden" id="width" name="width" value="{{json_encode($width)}}">
        <input type="hidden" id="height" name="height" value="{{json_encode($height)}}">
        <input type="hidden" name="{{$config_name}}" value="{{$config_original}}">
        <input type="hidden" name="{{$file_manager_id_name}}" value="{{$file_manager_id}}">
        <div id="crop_preview"></div>
        <div id="crop_value"></div>

        <script src="{{ config('constant.ASSETS_URL') }}backend/js/jQueryAutoHeight.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.12/js/jquery.Jcrop.min.js"></script>
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/0.9.12/css/jquery.Jcrop.min.css" />
        <script src="{{ config('constant.ASSETS_URL') }}backend/js/crop_gallery.js"></script>
        <script>
        var image_data = document.getElementById('image_data').value;
        var komponen = document.getElementById('komponen').value;
        var width = document.getElementById('width').value;
        var height = document.getElementById('height').value;

        master_crop(image_data,komponen,width,height);
        $(document).ready(function(){
            alert('abc')
            $('.image-box').autoHeight({column:1,clear:1});
        });
        </script>
    @else
        <div>
            <img src="{{config('constant.ASSETS_URL').'uploads/img/original/'.$filename}}" width="200px">
        </div>
        <input type="hidden" name="{{$file_manager_id_name}}" value="{{$file_manager_id}}">
        <input type="hidden" name="{{$container_image}}" value="{{$filename}}">
        <input type="hidden" name="crop" value="false">
        <input type="hidden" name="{{$config_name}}" value="{{$config_original}}">
    @endif
@else
    <div>
        <img src="{{config('constant.ASSETS_URL').'uploads/img/original/'.$filename}}"><br>
        {{$caption}}
    </div>
@endif