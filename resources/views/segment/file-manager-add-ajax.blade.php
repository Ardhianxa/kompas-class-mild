<form method="post" class="needs-validation file-manager-form-add" id="file-manager-form-add" enctype= multipart/form-data action="javascript:void(0)">
    @csrf
    <div class="card-body">
    <div class="row">                               
        <div class="form-group col-sm-12 col-md-12 col-12">
            <label>Name</label>
            <input type="text" class="form-control" name="title" value="" required="">
        </div>
        <div class="form-group col-sm-12 col-md-12 col-12">
            <label>Caption</label>
            <textarea class="form-control" name="caption" rows="20" cols="10"></textarea>
        </div>
        <div class="form-group col-sm-12 col-md-12 col-12">
            <label>Note</label>
            <textarea class="form-control" name="note" rows="20" cols="10"></textarea>
        </div>
        <div class="form-group col-sm-12 col-md-12 col-lg-12 pull-left" align="left">
            <label>Share</label><br>
            <div class="big-checkbox">
            <input type="checkbox" name="share" class="big-checkbox" value="1">
            </div>
        </div>
        <div class="form-group col-sm-12 col-md-12 col-lg-12 pull-left" align="left">
            <label>Watermark</label><br>
            <div class="big-checkbox">
            <input type="checkbox" name="watermark" class="big-checkbox" value="1">
            <select class="form-control" name="watermark_position">
                <option value="top-left">TOP LEFT</option>
                <option value="top-center">TOP CENTER</option>
                <option value="top-right">TOP RIGHT</option>
                <option value="center-left">CENTER LEFT</option>
                <option value="center-center">CENTER CENTER</option>
                <option value="center-right">CENTER RIGHT</option>
                <option value="bottom-left">BOTTOM LEFT</option>
                <option value="bottom-center">BOTTOM CENTER</option>
                <option value="bottom-right">BOTTOM RIGHT</option>
            </select> 
            </div>
        </div>
        <div class="form-group col-sm-12 col-md-12 col-12">
            <label>Image</label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail">
                    <img width="100px" name="" foto src="{{ config('constant.ASSETS_URL').'backend/img/avatar/avatar-1.png' }}" alt="" />
                </div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100%; max-height: 150px; line-height: 20px;"></div>
                <div>
                    <span class="btn btn-file btn-primary">
                        <span class="fileupload-new">Select image</span>
                        <span class="fileupload-exists">Change</span>
                        <input accept="image/*" type="file" name="upfile" id="upfile"/>
                    </span>
                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                    <div class="notif-upload">
                        Max Size : {{config('constant.MAX_SIZE_IMAGE_UPLOAD')}} MB
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-sm-12 col-md-12 col-12" align="right">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
    </div>
    </div>
    <input type="hidden" name="type" value="image">
    <input type="hidden" id="config" value="{{$config}}">
</form>
<script>
$(document).ready(function() {
  $('form.file-manager-form-add').submit(function(event) {
    let formData = new FormData($('#file-manager-form-add')[0]);
    var error = false;
    if( document.getElementById("upfile").files.length != 0 ){
        let file = $('input[type=file]')[0].files[0];
        let size = file.size;
        formData.append('upfile', file, file.name);

        var limit_size = "{{config('constant.MAX_SIZE_IMAGE_UPLOAD')*1000000}}";
        if(parseInt(size) > parseInt(limit_size)){
            error = true;
        }
    }

    if(error){
        swal("Oppss...", "Ukuran maksimal adalah : "+parseInt(limit_size)/1000000+" MB", "warning");
    }
    else{
        var config = document.getElementById('config').value;
        $.ajax({
        type        : 'POST',
        url         : "{{route('file-manager-store')}}",
        contentType: false,
        processData: false,   
        cache: false,      
        data        : formData,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function (request, error) {
        },
        success: function (response) {
            if(response=="ok"){
                $('.modal-add-image').modal('toggle');
                file_manager_data("{{ route('file-manager-get',['type' => 'image','page' => 1, 'order' => 'desc']) }}",'image',config);
            }
            else{
                swal("Oppss...", response, "warning");
            }
        }
        });
    }
    event.preventDefault();
  });
});
</script>
