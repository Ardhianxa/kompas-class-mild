@include('segment.file-manager-filter',['type' => 'video'])
@if(count($data) > 0)
    @foreach($data as $key => $value)
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="box-image" align="center">
                <iframe width="200" height="100" src="https://www.youtube.com/embed/{{$value->filename}}?controls=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br>
                <div class="box-image-title">{{$value->title}}</div>
                <a data-toggle="modal" data-target=".modal-detail" style="width:100%;margin-top:5px;" href="#" class="btn btn-sm btn-primary">Detail</a>
            </div>
        </div>
    @endforeach
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="container-video">
            {{ $data->links('pagination.file-manager') }}
        </div>
    </div>
@endif
@include('segment.file-manager-add',['type' => 'video'])