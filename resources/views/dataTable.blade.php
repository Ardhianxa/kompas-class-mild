<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">

<script src="{{ config('constant.ASSETS_URL') }}backend//modules/datatables/datatables.min.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend//modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend//modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend//modules/jquery-ui/jquery-ui.min.js"></script>
<script>
    var dataTable = $('.dataTable-{{$status2}}').DataTable( {
        "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    dataTable.on( 'order.dt search.dt', function () {
        dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
</script>