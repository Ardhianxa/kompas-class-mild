@extends('layouts.app') 

@section('extend_code')
  <!-- <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>  
  <script>
    var route_prefix = "url-to-filemanager";
    
    $('#lfm').filemanager('image', {prefix: 'filemanager'});
  </script>  -->
@endsection

@section('content')
<!-- <div class="input-group">
   <span class="input-group-btn">
     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input id="thumbnail" class="form-control" type="text" name="filepath">
 </div>
 <img id="holder" style="margin-top:15px;max-height:100px;"> -->

<div class="section-body">
  <h2 class="section-title">Hi, {{ Session::get('first_name').' '.Session::get('last_name') }}!</h2>
  <p class="section-lead">
    Change information about yourself on this page.
  </p>
  <div class="row mt-sm-4">
    <div class="col-12 col-md-12 col-lg-5">
      <div class="card profile-widget">
        <div class="profile-widget-header">  
          <a data-fancybox="gallery" href="{{ Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.Session::get('image')) }}">                    
            <img alt="image" src="{{ Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.Session::get('image')) }}" class="rounded-circle profile-widget-picture">
          </a>  
          <div class="profile-widget-items">
            <div class="profile-widget-item">
              <div class="profile-widget-item-label">Register Date</div>
              <div class="profile-widget-item-value">{{ Session::get('register_date') }}</div>
            </div>
            <div class="profile-widget-item">
              <div class="profile-widget-item-label">Last Login</div>
              <div class="profile-widget-item-value">{{ Session::get('last_login') }}</div>
            </div>
            <div class="profile-widget-item">
              <div class="profile-widget-item-label">Privileges</div>
              <div class="profile-widget-item-value">
                @php
                  $i = 1;    
                @endphp
                @foreach($privileges as $privileges_value)
                  {{ $privileges_value }}
                  @php
                    if($i < count($privileges)){
                      echo ',';
                    }
                    $i++;
                  @endphp
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="profile-widget-description"></div>
      </div>
    </div>
    <div class="col-12 col-md-12 col-lg-7">
      <div class="card">
        <form method="post" id="form-file-manager-data" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('post.profile') }}">
          @csrf
          <div class="card-body">
              <div class="row">                               
                <div class="form-group col-md-6 col-12">
                  <label>First Name</label>
                  <input type="text" class="form-control" name="first_name" value="{{ Session::get('first_name') }}" required="">
                  <div class="invalid-feedback">
                    Please fill in the first name
                  </div>
                </div>
                <div class="form-group col-md-6 col-12">
                  <label>Last Name</label>
                  <input type="text" class="form-control" name="last_name" value="{{ Session::get('last_name') }}" required="">
                  <div class="invalid-feedback">
                    Please fill in the last name
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-7 col-12">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" value="{{ Session::get('email') }}" required="" readonly>
                  <div class="invalid-feedback">
                    Please fill in the email
                  </div>
                </div>
                <div class="form-group col-md-5 col-12">
                  <label>Phone</label>
                  <input type="tel" class="form-control" name="handphone" value="{{ Session::get('handphone') }}" required="">
                  <div class="invalid-feedback">
                    Please fill phone number
                  </div>
                </div>
                <div class="form-group col-md-6 col-12">
                  <label>Password</label>
                  <input type="password" class="form-control" placeholder="Kosongkan Jika Tidak Mengganti Password" name="password" value="">
                  <div class="invalid-feedback">
                    Please fill in the email
                  </div>
                </div>
                <div class="form-group col-md-6 col-12">
                  <label>Re-Type Password</label>
                  <input type="password" class="form-control" placeholder="Kosongkan Jika Tidak Mengganti Password" name="repassword" value="">
                  <div class="invalid-feedback">
                    Please fill in the email
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-5 col-12">
                  <label>Photo</label>
                  <br>
                  
                  <a data-fancybox="gallery" href="{{ Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.Session::get('image')) }}">
                    <img src="{{Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.Session::get('image'))}}" width="200px"><br><br>
                  </a>
                  <div class="file-manager-data"></div><br>
                  <a href="#" onclick="file_manager_show('{{config('constant.CONFIG_IMAGE_PROFILE')}}')"
                  class="btn btn-md btn-primary">Select Image</a>
                  <a href="#" class="btn btn-md btn-danger" onclick="clear_box_image('{{config('constant.CONFIG_IMAGE_PROFILE')}}')">Remove</a>
                </div>
              </div>
          </div>
          <div class="card-footer text-right">
            <input type="submit" class="btn btn-primary" value="Save">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection