@extends('layouts.app') 

@section('content')
<section class="section">
  @include('layouts.section_header')
  <div class="section-body">
    <div class="row">
      <div class="col-12">
        <div class="row">
          <div class="col-2">
            <div class="form-group">
              <label>Start</label>
              <input type="text" id="start" class="date form-control" value="{{ $start }}">
            </div>
          </div>
          <div class="col-2">
            <div class="form-group">
              <label>End</label>
              <input type="text" id="end" class="date form-control" value="{{ $end }}">
            </div>
          </div>
          <div class="col-2">
            <div class="form-group">
              <label>&nbsp;</label>
              <a onclick="redirect('{{ config('constant.APP_URL') }}activities/'+document.getElementById('start').value+'/'+document.getElementById('end').value)" href="#" class="btn btn-primary form-control">Search</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section-body">
    @foreach($activities as $activities_value)
      <h2 class="section-title">{{ date_without_hours($activities_value->tanggal) }}</h2>
      <div class="row">
        <div class="col-12">
          <div class="activities">
            @foreach($activities_value->activity as $activity)
            <div class="activity">
              <div class="activity-icon bg-primary text-white shadow-primary">
                <i class="fas {{ $activity->icon }}"></i>
              </div>
              <div class="activity-detail">
                <div class="mb-2">
                  <span class="text-job text-primary">{{ hours($activity->created_at) }}</span>
                  <span class="bullet"></span>
                </div>
                <p>
                  {{ $activity->name }}
                  @if($activity->activity != '')
                   - {{ $activity->activity }}
                  @endif
                  @if($activity->content_id != '')
                   - Content ID : {{ $activity->content_id }}
                  @endif
                </p>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    @endforeach 
  </div>
</section>
@endsection
