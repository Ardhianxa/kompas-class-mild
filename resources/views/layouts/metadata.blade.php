<meta charset="UTF-8">
<link rel="shortcut icon" type="image/png" href="https://www.itsolutionstuff.com/frontTheme/images/logo.png"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="description" content="This is an example dashboard created using build-in elements and components.">
<meta name="msapplication-tap-highlight" content="no">
<meta name="csrf-token" content="{{ csrf_token() }}">