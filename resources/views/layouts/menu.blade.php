<div class="main-sidebar sidebar-style-2"> 
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home-admin') }}">Clas-Mild</a>  
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home-admin') }}">Mild</a>
    </div>
    <ul class="sidebar-menu">

      
      <li class="menu-header">Dashboard</li>

      <li class="@if(strtolower(Request::segment(1))=='home') active @endif"><a class="nav-link" href="{{ route('home-admin') }}"><i class="far fa-user"></i> <span>Dashboard</span></a></li>
      
      <li class="menu-header">Users</li>

      @if(auth()->user()->can('view users'))
        <li class="@if(strtolower(Request::segment(1))=='users') active @endif"><a class="nav-link" href="{{ route('users',['status' => 'active']) }}"><i class="far fa-user"></i> <span>Users</span></a></li>
      @endif

      @if(auth()->user()->can('view privileges'))
        <li class="@if(strtolower(Request::segment(1))=='privileges') active @endif"><a class="nav-link" href="{{ route('privileges',['status' => 'active']) }}"><i class="fas fa-universal-access"></i> <span>Privileges</span></a></li>
      @endif

      @if(request()->getHttpHost()=="local.classmild.id" || request()->getHttpHost()=="clasmild.dev.grid.co.id")
        <li class="menu-header">Content</li>
        @if(auth()->user()->can('view consumer letter periklanan') || auth()->user()->can('view consumer letter ketersediaan produk') || auth()->user()->can('view consumer letter kualitas produk'))
          <li class="@if(strtolower(Request::segment(1))=='consumer-letter') active @endif"><a class="nav-link" href="{{ route('consumer-letter',['status' => 'new']) }}"><i class="fas fa-mail-bulk"></i> <span>Suara Konsumen</span></a></li>
        @endif

        @if(auth()->user()->can('view screening consumer data'))
          <li class="@if(strtolower(Request::segment(1))=='screening-data') active @endif"><a class="nav-link" href="{{ route('screening-data') }}"><i class="fas fa-mail-bulk"></i> <span>Screening Data</span></a></li>
        @endif
      @endif

      @if(request()->getHttpHost()=="local.classmild.id")
        @if(auth()->user()->can('view article'))
          <li class="@if(strtolower(Request::segment(1))=='article') active @endif"><a class="nav-link" href="{{ route('article',['status' => 'active']) }}"><i class="fas fa-newspaper"></i> <span>Article</span></a></li>
        @endif
      @endif

      @if(request()->getHttpHost()=="local.classmild.id")
        <li class="menu-header">Dsitribution</li>
        @if(auth()->user()->can('view channel'))
          <li class="@if(strtolower(Request::segment(1))=='channel') active @endif"><a class="nav-link" href="{{ route('channel') }}"><i class="fas fa-tags"></i> <span>Channel</span></a></li>
        @endif
      @endif
    </ul>        
  </aside>
</div>