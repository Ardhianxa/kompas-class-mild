import Vue from 'vue'
import Router from 'vue-router'
import Login from './Login.vue'
import App from './App.vue'


Vue.component("app", App);
Vue.use(Router)

//DEFINE ROUTE
const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/cms_new/public/test',
            name: 'login',
            component: Login,
        },
    ]
});

export default router