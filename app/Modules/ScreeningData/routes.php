<?php
Route::group(  
[
	'namespace' => 'App\Modules\ScreeningData\Controllers', 
	'prefix' => '', 
	'middleware' => [
						'web',
						'session',
						'auth',
						'before_global',
						'after_global',
						'App\Modules\ScreeningData\Middleware\BeforeMiddleware',
						'App\Modules\ScreeningData\Middleware\AfterMiddleware'
					]
], 
function ()
{	
	Route::get('screening-data/{key?}', [
		'as' => 'screening-data', 'uses' => 'ScreeningController@index', 'middleware' => ['permission:view screening consumer data']
	]);
});
?>