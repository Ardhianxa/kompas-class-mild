@extends('layouts.app') 

@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills" id="myTab3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="active-data" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="true">Active</a>
                    </li>
                    </ul>
                    <div class="tab-content" id="myTabContent2">
                        <div class="tab-pane fade show active" id="active" role="tabpanel" aria-labelledby="active-data">
                            <div class="card-body p-0 box-table-search">
                                <div class="col-12 col-md-12 col-lg-3">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="search-active" placeholder="Search" aria-label="" value="{{ $key }}">
                                        <div class="input-group-append">
                                        <a onclick="redirect('{{ config('constant.APP_URL') }}/screening-data/'+document.getElementById('search-active').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Perokok</th>
                                    <th>Merek Rokok</th>
                                </tr>
                                @php
                                $no=1;
                                @endphp
                                @if(count($active) > 0)
                                @foreach($active as $active_value)
                                    <tr>
                                        <td>{{ ($no+($active->perPage()*($active->currentPage()-1))) }}</td>
                                        <td>{{ date_with_hours($active_value->tanggal_lahir) }}</td>
                                        <td>
                                            <div style="font-size:30px;">
                                                @if($active_value->perokok == 1)
                                                &#10003;
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{ $active_value->merek_rokok }}</td>
                                    </tr>
                                @php
                                $no++;
                                @endphp
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="7"><div align="center">-No Data-</div></td>
                                    </tr>
                                @endif
                            </table>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                {{ $active->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection