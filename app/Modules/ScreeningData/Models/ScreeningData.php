<?php
namespace App\Modules\ScreeningData\Models; 

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class ScreeningData extends Model
{
    protected $table = 'screening_frontend';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public static function data($request,$key)
    {
        $cache_name = json_encode($request->all());
        if(config('constant.CACHE')){
            $cache_name = json_encode($request->all()).'_'.$key;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::data_data($request,$key), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::data_data($request,$key);
        }

        return $data;
    }

    public static function data_data($request,$key)
    {
        $data = Self::
        select('id','tanggal_lahir','perokok','merek_rokok')
        ->whereRaw("
            (
                (merek_rokok LIKE '%".$key."%')
            )
        ")->orderBy('created_at','desc')->paginate(config('constant.LIMIT_DATA_TABLE'));

        return $data;
    }
}
