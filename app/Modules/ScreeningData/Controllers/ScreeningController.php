<?php
 
namespace App\Modules\ScreeningData\Controllers;

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Modules\ScreeningData\Models\ScreeningData;
use Auth;

class ScreeningController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request, $key = null)
    {
        $title = 'Screening Data';
        $page = 'ScreeningData::data';
        $active = ScreeningData::data($request,$key);

        return view($page,compact('title','page','active','key'));
    }
}
