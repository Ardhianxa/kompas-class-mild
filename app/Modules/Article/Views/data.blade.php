@extends('layouts.app') 

@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills" id="myTab3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link @if($status=='active') active @endif" id="active-data" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="true">Active</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if($status=='draft') active @endif" id="draft-data" data-toggle="tab" href="#draft" role="tab" aria-controls="draft" aria-selected="false">Draft</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if($status=='inactive') active @endif" id="inactive-data" data-toggle="tab" href="#inactive" role="tab" aria-controls="inactive" aria-selected="false">Inactive</a>
                    </li>
                    </ul>
                    <div class="tab-content" id="myTabContent2">
                        <div class="tab-pane fade show @if($status=='active') active @endif" id="active" role="tabpanel" aria-labelledby="active-data">
                            <div class="card-body p-0 box-table-search">
                                <!-- <div class="col-12 col-md-12 col-lg-3">
                                    <div class="accordion" id="accordion">  
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" id="search-active" placeholder="Search" aria-label="" value="@if($status=='active'){{ $key }}@endif">
                                            <div class="input-group-append">
                                                <a onclick="redirect('{{ config('constant.APP_URL') }}/article/active/'+document.getElementById('search-active').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
                                                <div class="accordion-header" style="width:93%;" role="button" id="collapse-panel" data-toggle="collapse" data-target="#panel-body-search" aria-expanded="false">
                                                    <div class="expand">
                                                        <i class="fas fa-expand"></i>
                                                    </div>
                                                    <h4>Advance Search</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-header" style="width:93%;" role="button" id="collapse-panel" data-toggle="collapse" data-target="#panel-body-search" aria-expanded="false">
                                            <div class="expand">
                                                <i class="fas fa-expand"></i>
                                            </div>
                                            <h4>Advance Search</h4>
                                        </div>
                                        <div style="margin-bottom:70px;width:93%;" class="accordion-body accordion-body-1 collapse" id="panel-body-search" data-parent="#accordion">
                                            <div class="accordion" id="accordion2">
                                                ARDI
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-12 col-md-12 col-lg-3">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" id="search-active" placeholder="Search" aria-label="" value="@if($status=='active'){{ $key }}@endif">
                                            <div class="input-group-append">
                                            <a onclick="redirect('{{ config('constant.APP_URL') }}/article/active/'+document.getElementById('search-active').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Channel</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Created Date</th>
                                    <th>Publish Date</th>
                                    <th width="5%">@if(auth()->user()->can('add article'))<a href="{{ route('article-add') }}" class="btn btn-md btn-primary">Create New</a>@endif</th>
                                </tr>
                                @php
                                $no=1;
                                @endphp
                                @if(count($active) > 0)
                                @foreach($active as $active_value)
                                    <tr>
                                        <td>{{ ($no+($active->perPage()*($active->currentPage()-1))) }}</td>
                                        <td>{{ channel($active_value->channel) }}</td>
                                        <td>{{ $active_value->title }}</td>
                                        <td>{{ $active_value->author->first_name.' '.$active_value->author->last_name }}</td>
                                        <td>{{ date_with_hours($active_value->created_at) }}</td>
                                        <td>{{ date_with_hours($active_value->publish_date) }}</td>
                                        <td>
                                            @if(auth()->user()->can('edit article') && $active_value->user_id == Auth::id())
                                                <a class="btn btn-icon btn-primary" href='{{ route("article-edit",["status" => "active", "id" => $active_value->id]) }}'><i class="far fa-edit"></i></a>
                                            @endif
                                            @if(auth()->user()->can('delete article') && $active_value->user_id == Auth::id())
                                                <a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("article-delete",["status" => "active", "id" => $active_value->id]) }}")'><i class="fas fa-times"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @php
                                $no++;
                                @endphp
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="7"><div align="center">-No Data-</div></td>
                                    </tr>
                                @endif
                            </table>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                {{ $active->links() }}
                                </nav>
                            </div>
                        </div>
                        <div class="tab-pane fade show @if($status=='draft') active @endif" id="draft" role="tabpanel" aria-labelledby="draft-data">
                            <div class="card-body p-0 box-table-search">
                                <div class="col-12 col-md-12 col-lg-3">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" id="search-draft" placeholder="Search" aria-label="" value="@if($status=='draft'){{ $key }}@endif">
                                            <div class="input-group-append">
                                            <a onclick="redirect('{{ config('constant.APP_URL') }}/article/draft/'+document.getElementById('search-draft').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                <table class="table table-striped table-md">
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Channel</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Created Date</th>
                                        <th>Publish Date</th>
                                    </tr>
                                    @php
                                    $no=1;
                                    @endphp
                                    @if(count($draft) > 0)
                                    @foreach($draft as $draft_value)
                                    <tr>
                                        <td>{{ ($no+($draft->perPage()*($draft->currentPage()-1))) }}</td>
                                        <td>{{ channel($draft_value->channel) }}</td>
                                        <td>{{ $draft_value->title }}</td>
                                        <td>{{ $draft_value->author->first_name.' '.$draft_value->author->last_name }}</td>
                                        <td>{{ date_with_hours($draft_value->created_at) }}</td>
                                        <td>{{ date_with_hours($draft_value->publish_date) }}</td>
                                        <td>
                                            @if(auth()->user()->can('edit article') && $draft_value->user_id == Auth::id())
                                                <a class="btn btn-icon btn-primary" href='{{ route("article-edit",["status" => "draft", "id" => $draft_value->id]) }}'><i class="far fa-edit"></i></a>
                                            @endif

                                            @if(auth()->user()->can('delete article') && $draft_value->user_id == Auth::id())
                                                <a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("article-delete",["status" => "draft", "id" => $draft_value->id]) }}")'>Delete</a>
                                            @endif
                                    </td>
                                    </tr>
                                    @php
                                    $no++;
                                    @endphp
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7"><div align="center">-No Data-</div></td>
                                        </tr>
                                    @endif
                                </table>
                                </div>
                                <div class="card-footer text-right">
                                    <nav class="d-inline-block">
                                    {{ $draft->links() }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show @if($status=='inactive') active @endif" id="inactive" role="tabpanel" aria-labelledby="inactive-data">
                            <div class="card-body p-0 box-table-search">
                                <div class="col-12 col-md-12 col-lg-3">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="search-inactive" placeholder="Search" aria-label="" value="@if($status=='inactive'){{ $key }}@endif">
                                        <div class="input-group-append">
                                            <a onclick="redirect('{{ config('constant.APP_URL') }}/article/inactive/'+document.getElementById('search-inactive').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                <table class="table table-striped table-md">
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Channel</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Created Date</th>
                                        <th>Publish Date</th>
                                    </tr>
                                    @php
                                    $no=1;
                                    @endphp
                                    @if(count($inactive) > 0)
                                    @foreach($inactive as $inactive_value)
                                    <tr>
                                        <td>{{ ($no+($inactive->perPage()*($inactive->currentPage()-1))) }}</td>
                                        <td>{{ channel($inactive_value->channel) }}</td>
                                        <td>{{ $inactive_value->title }}</td>
                                        <td>{{ $inactive_value->author->first_name.' '.$inactive_value->author->last_name }}</td>
                                        <td>{{ date_with_hours($inactive_value->created_at) }}</td>
                                        <td>{{ date_with_hours($inactive_value->publish_date) }}</td>
                                        <td>
                                            @if(auth()->user()->can('delete article') && $inactive_value->user_id == Auth::id())
                                                <a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("article-delete",["status" => "inactive", "id" => $inactive_value->id]) }}")'>Restore</a>
                                            @endif
                                    </td>
                                    </tr>
                                    @php
                                    $no++;
                                    @endphp
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7"><div align="center">-No Data-</div></td>
                                        </tr>
                                    @endif
                                </table>
                                </div>
                                <div class="card-footer text-right">
                                    <nav class="d-inline-block">
                                    {{ $inactive->links() }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection