@extends('layouts.app') 

@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <form method="post" id="form-file-manager-data" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('article-add-post') }}">
          @csrf
            <div class="card-body">
              	<div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Title</label>
                    	<input type="text" class="form-control" name="title" value="">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Channel</label>
                    	<select required name="channel_id" id="channel" class="form-control">
                            <option value="">-Choose-</option>
                            @foreach($channel as $channel_key => $channel_value)
                                <!--LEVEL 1-->
                                @if(count($channel_value->child)==0)
                                    <option value="{{$channel_value->id}}">{{$channel_value->name}}</option>
                                @else
                                    <optgroup label="{{$channel_value->name}}">
                                        <!--LEVEL 2-->
                                        @foreach($channel_value->child as $channel_value2)
                                            @if(count($channel_value2->child)==0)
                                                <option value="{{$channel_value2->id}}">{{$channel_value2->name}}</option>
                                            @else
                                                <optgroup label="{{$channel_value2->name}}">
                                                    <!--LEVEL 3-->
                                                    @foreach($channel_value2->child as $channel_value3)
                                                        @if(count($channel_value3->child)==0)
                                                            <option value="{{$channel_value3->id}}">{{$channel_value3->name}}</option>
                                                        @else
                                                            <optgroup label="{{$channel_value3->name}}">
                                                                <!--LEVEL 4-->
                                                                @foreach($channel_value3->child as $channel_value4)
                                                                    <option value="{{$channel_value4->id}}">{{$channel_value4->name}}</option>
                                                                @endforeach
                                                            </optgroup>
                                                        @endif
                                                    @endforeach
                                                </optgroup>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endif
                                
                            @endforeach
                        </select>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Summary</label>
                    	<textarea class="form-control" rows="5" name="summary" cols="20" style="height:100% !important;"></textarea>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Publish Date</label>
                    	<input type="text" class="form-control datetimepicker" name="publish_date">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group col-sm-12 col-md-12 col-lg-3"> 
                                <label>Comment</label><br>
                                <label class='custom-switch mt-2'>
                                    <input type='checkbox' value='1' name='comment' class='custom-switch-input' checked style='width:200px;'>
                                    <span class='custom-switch-indicator'></span><span class='custom-switch-description'>Active</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                        <br>
                    	<label>Video</label>
                    	<input type="text" class="form-control" name="video" value="">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                        <br>
                    	<label>Tagging</label>
                    	<select class="js-example-tags form-control" name="tagging[]" id="tagging" multiple="multiple"></select>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label>Content</label>
                        <textarea class="summernote" name="body"></textarea>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-2">
                        <label>Cover</label><br>
                        <div class="file-manager-data-default-image">
                            <img src="{{config('constant.ASSETS_URL')}}backend/img/example-image.jpg" width="200px">
                        </div>
                        <div class="file-manager-data"></div><br>
                        <a href="#" onclick="file_manager_show('{{config('constant.CONFIG_IMAGE_ARTICLE_COVER')}}')" class="btn btn-md btn-primary">Select Image</a>
                        <a href="#" class="btn btn-md btn-danger" onclick="clear_box_image('{{config('constant.CONFIG_IMAGE_ARTICLE_COVER')}}')">Remove</a>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-1">
                        <label>Other Image</label><br>
                        <a class="btn btn-lg btn-success" onclick="article_add_other_image('{{route('article-add-other-image')}}','container-other-image')" style="font-size:20px;">+</a>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-9">
                        <div class="row container-other-image">
                            <label>&nbsp</label><br>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>
@endsection