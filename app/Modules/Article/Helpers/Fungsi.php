<?php
if(!function_exists('channel_slug')){ 
    function channel_slug($channel)
    {
        return str_replace(" ","-",strtolower($channel));
    }
}
if(!function_exists('channel')){ 
    function channel($channel)
    {
        $result = "";
        foreach($channel as $channel_key => $channel_value){
            if($channel_key==0){
                $result = $channel_value['name'];
            }
            else{
                $result = $result.' > '.$channel_value['name'];
            }
        }
        return $result;
    }
}

?>