<?php
 
namespace App\Modules\Article\Controllers;

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Modules\Article\Models\Article;
use App\Modules\Article\Models\Tagging;
use App\Modules\Article\Models\Content_tagging;
use App\Modules\Article\Models\Content_gallery;
use App\Modules\Channel\Models\Channel;
use Auth;

class ArticleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request, $status, $key = null)
    {
        $title = 'Article';
        $page = 'Article::data';
        $active = Article::data($request,1,$key);
        $draft = Article::data($request,2,$key);
        $inactive = Article::data($request,0,$key);

        return view($page,compact('title','page','active','inactive','draft','status','key'));
    }

    public function update(Request $request)
    {
        $status = $request->status;
        $artikel = Article::detail($request->id);
        if(!isset($artikel->id)){
            return view('errors.404');
        }
        $artikel->title = $request->title;
        $artikel->channel_id = $request->channel_id;
        $artikel->summary = $request->summary;
        $artikel->publish_date = date('Y-m-d H:i:s',strtotime($request->publish_date));
        $artikel->comment = $request->comment;
        $artikel->video = $request->video;
        $artikel->body = $request->body;
        if($artikel->save()){
            //save tagging
            $tagging = Tagging::savedata($request->id,$request->tagging);

            //cover
            if($request->config != "")
            {
                $container_image = Helpers::get_container_image($request->config);
                $image = Helpers::file_manager_image($request->$container_image,$request);
                if($image != "" && $image != 'error'){
                    //update cover
                    $cover = Content_gallery::select('id')->where('type',1)->where('content_id',$request->id)->first();
                    if(isset($cover->id)){
                        $cover->file_manager_id = $request->file_manager_id;
                        $cover->save();
                    }
                    else{
                        //insert cover
                        $cover = new Content_gallery();
                        $cover->file_manager_id = $request->file_manager_id;
                        $cover->content_id = $request->id;
                        $cover->type = 1;
                        $cover->save();
                    }
                }
            }

            //gallery
            $delete = Content_gallery::select('id')->where('type',2)->where('content_id',$request->id)->delete();
            if(is_array($request->other_image))
            {
                if(count($request->other_image) > 0){
                    foreach($request->other_image as $other_image){
                        $config_name = "config_".$other_image;
                        $file_manager_id_name = "file_manager_id_".$other_image;
                        $container_image = Helpers::get_container_image($request->$config_name);
                        $image = Helpers::file_manager_image($request->$container_image,$request);
                        if($image != "" && $image != 'error'){
                            //insert cover
                            $cover = new Content_gallery();
                            $cover->file_manager_id = $request->$file_manager_id_name;
                            $cover->content_id = $request->id;
                            $cover->type = 2;
                            $cover->save();
                        }
                    }
                }
            }
            return redirect()->route('article',['status' => 'active'])->with('flash_success', 'Successfully');
        }
        else{
            return redirect()->route('article-add')->with('flash_failed', 'Internal Server Error');
        }
    }

    public function store(Request $request)
    {
        $data = new Article();
        $data->title = $request->title;
        $data->channel_id = $request->channel_id;
        $data->summary = $request->summary;
        $data->publish_date = date('Y-m-d H:i:s',strtotime($request->publish_date));
        $data->comment = $request->comment;
        $data->video = $request->video;
        $data->body = $request->body;
        $data->user_id = Auth::id();
        $data->status = 1;

        if($data->save()){
            //save tagging
            $tagging = Tagging::savedata($data->id,$request->tagging);

            //cover
            if($request->config != "")
            {
                $container_image = Helpers::get_container_image($request->config);
                $image = Helpers::file_manager_image($request->$container_image,$request);
                if($image != ""){
                    if($image != 'error'){
                        //insert cover
                        $cover = new Content_gallery();
                        $cover->file_manager_id = $request->file_manager_id;
                        $cover->content_id = $data->id;
                        $cover->type = 1;
                        $cover->save();
                    }
                }
            }

            //other image
            if(is_array($request->other_image))
            {
                if(count($request->other_image) > 0){
                    foreach($request->other_image as $other_image){
                        $config_name = "config_".$other_image;
                        $file_manager_id_name = "file_manager_id_".$other_image;
                        $container_image = Helpers::get_container_image($request->$config_name);
                        $image = Helpers::file_manager_image($request->$container_image,$request);
                        
                        if($image != ""){
                            if($image != 'error'){
                                //insert cover
                                $cover = new Content_gallery();
                                $cover->file_manager_id = $request->$file_manager_id_name;
                                $cover->content_id = $data->id;
                                $cover->type = 2;
                                $cover->save();
                            }
                        }
                    }
                }
            }
            return redirect()->route('article',['status' => 'active'])->with('flash_success', 'Successfully');
        }
        else{
            return redirect()->route('article-add')->with('flash_failed', 'Internal Server Error');
        }
    }

    public function add_other_image()
    {
        $div_id = uniqid();
        $config_modified = Helpers::config_modified(config('constant.CONFIG_IMAGE_ARTICLE_COVER'),array('file_manager_id_name' => 'file_manager_id_'.$div_id, 'config_name' => 'config_'.$div_id, 'container_image' => 'image_data_'.$div_id, 'container' => 'file-manager-data-'.$div_id));
        $page = 'Article::add_other_image';
        return view($page,compact('page','div_id','config_modified'));
    }

    public function add(Request $request)
    {
        $title = 'Add Article';
        $page = 'Article::add';
        $channel = Channel::data();

        return view($page,compact('title','page','channel'));
    }

    public function delete(Request $request, $status, $id)
    {
        $delete = Article::select('id')->where('id',$id)->first();

        $change = 2;
        if($status=='active' || $status=='draft'){
            $change = 0;
        }

        $delete->status = $change;
        $delete->save();
        $request->session()->flash('flash_success', 'Successfully');
        echo route('article',['status' => $status]);
    }

    public function edit(Request $request)
    {
        $title = 'Edit Article';
        $page = 'Article::edit';
        $status = $request->status;
        if($request->status != "active" && $request->status != "inactive" && $request->status != "draft"){
            return view('errors.404');
        }
        $data = Article::detail($request->id);
        if(!isset($data->id)){
            return view('errors.404');
        }
        $channel = Channel::data();
        
        return view($page,compact('title','page','channel','data','status'));
    }
}
