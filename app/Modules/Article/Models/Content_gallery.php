<?php
namespace App\Modules\Article\Models; 

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class Content_gallery extends Model
{
    protected $table = 'content_gallery';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public function File_manager()
    {
        return $this->hasOne('App\Http\Models\FileManager', 'id', 'file_manager_id')
            ->select(['id', 'filename']);
    }
}
