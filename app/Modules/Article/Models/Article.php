<?php
namespace App\Modules\Article\Models; 

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Channel\Models\Channel;
use App\Modules\Article\Models\Content_gallery;

class Article extends Model
{
    protected $table = 'article';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public static function detail($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "article-detail_".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::detail_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::detail_data($id);
        }

        return $data;
    }

    public static function detail_data($id)
    {
        return Self::select('id','title','summary','body','video','comment','publish_date','user_id','member_id','channel_id')
        ->with('Cover','Gallery','Tagging')
        ->where('id',$id)->first();
    }

    public static function data($request,$status,$key)
    {
        $cache_name = json_encode($request->all());
        if(config('constant.CACHE')){
            $cache_name = json_encode($request->all()).'_'.$status.'_'.$key;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::data_data($request,$status,$key), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::data_data($request,$status,$key);
        }

        return $data;
    }

    public static function data_data($request,$status,$key)
    {
        $data = Self::
        select('id','channel_id','title','created_at','publish_date','user_id')
        ->with('Author')
        ->where('status',$status)
        ->where('user_id','!=',null)
        ->where('member_id',null)
        ->whereRaw("
            (
                (title LIKE '%".$key."%') or 
                (summary LIKE '%".$key."%') or 
                (body LIKE '%".$key."%')
            )
        ")->orderBy('created_at','desc')->paginate(config('constant.LIMIT_DATA_TABLE'));

        $data->transform(function($data){
            $data->channel = Self::channel($data->channel_id);
            return $data;
        });
        
        return $data;
    }

    public static function channel($channel_id)
    {
        $result = array();
        $data = Channel::select('level','parent_id','id','name')->where('id',$channel_id)->first();
        if($data->level==1){
            $value = array();
            $value['level'] = 1;
            $value['id'] = $data->id;
            $value['name'] = $data->name;
            $value['slug'] = channel_slug($data->name);
            $result[] = $value;
        }
        elseif($data->level==2){
            $data1 = Channel::select('id','name')->where('id',$data->parent_id)->first();

            //level 1
            $result2 = array();
            $result2['level'] = 1;
            $result2['id'] = $data1->id;
            $result2['name'] = $data1->name;
            $result2['slug'] = channel_slug($data1->name);
            $result[] = $result2;

            //level 2
            $value = array();
            $value['level'] = 2;
            $value['id'] = $data->id;
            $value['name'] = $data->name;
            $value['slug'] = channel_slug($data1->name).'/'.channel_slug($data->name);
            $result[] = $value;
        }
        elseif($data->level==3){
            $data2 = Channel::select('parent_id','id','name')->where('id',$data->parent_id)->first();
            $data1 = Channel::select('id','name')->where('id',$data2->parent_id)->first();

            //level 1
            $result1 = array();
            $result1['level'] = 1;
            $result1['id'] = $data1->id;
            $result1['name'] = $data1->name;
            $result1['slug'] = channel_slug($data1->name);
            $result[] = $result1;

            //level 2
            $result2 = array();
            $result2['level'] = 2;
            $result2['id'] = $data2->id;
            $result2['name'] = $data2->name;
            $result2['slug'] = channel_slug($data1->name).'/'.channel_slug($data2->name);
            $result[] = $result2;

            //level 3
            $result3 = array();
            $result3['level'] = 3;
            $result3['id'] = $data->id;
            $result3['name'] = $data->name;
            $result3['slug'] = channel_slug($data1->name).'/'.channel_slug($data2->name).'/'.channel_slug($data->name);
            $result[] = $result3;
        }
        elseif($data->level==4){
            $data3 = Channel::select('parent_id','id','name')->where('id',$data->parent_id)->first();
            $data2 = Channel::select('id','name','parent_id')->where('id',$data3->parent_id)->first();
            $data1 = Channel::select('id','name')->where('id',$data2->parent_id)->first();

            //level 1
            $result1 = array();
            $result1['level'] = 1;
            $result1['id'] = $data1->id;
            $result1['name'] = $data1->name;
            $result1['slug'] = channel_slug($data1->name);
            $result[] = $result1;

            //level 2
            $result2 = array();
            $result2['level'] = 2;
            $result2['id'] = $data2->id;
            $result2['name'] = $data2->name;
            $result2['slug'] = channel_slug($data1->name).'/'.channel_slug($data2->name);
            $result[] = $result2;

            //level 3
            $result3 = array();
            $result3['level'] = 3;
            $result3['id'] = $data3->id;
            $result3['name'] = $data3->name;
            $result3['slug'] = channel_slug($data1->name).'/'.channel_slug($data2->name).'/'.channel_slug($data3->name);
            $result[] = $result3;

            //level 4
            $result4 = array();
            $result4['level'] = 4;
            $result4['id'] = $data->id;
            $result4['name'] = $data->name;
            $result4['slug'] = channel_slug($data1->name).'/'.channel_slug($data2->name).'/'.channel_slug($data3->name).'/'.channel_slug($data->name);
            $result[] = $result4;
        }

        return $result;
    }

    public function Author()
    {
        return $this->hasOne('App\Http\Models\UsersInfo', 'user_id', 'user_id')
            ->select(['id', 'user_id', 'first_name','last_name']);
    }

    public function Gallery()
    {
        return $this->hasMany('App\Modules\Article\Models\Content_gallery', 'content_id', 'id')
            ->select(['id', 'content_id', 'file_manager_id','type'])
            ->where('type',2)
            ->with('File_manager')
            ->orderBy('type','asc');
    }

    public function Cover()
    {
        return $this->hasOne('App\Modules\Article\Models\Content_gallery', 'content_id', 'id')
            ->select(['id', 'content_id', 'file_manager_id','type'])
            ->where('type',1)
            ->with('File_manager')
            ->orderBy('type','asc');
    }

    public function Tagging()
    {
        return $this->hasMany('App\Modules\Article\Models\Content_tagging', 'content_id', 'id')
            ->select(['id', 'content_id', 'tagging_id'])->with('Data');
    }
}
