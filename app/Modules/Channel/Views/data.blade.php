@extends('layouts.app')     

@section('modal-data')
    @include('Channel::modal-form')
@endsection

@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                @if(auth()->user()->can('add channel'))
                    <div align="right">
                        <a href="#" class="btn btn-md btn-primary" onclick="channel_add(0,null,1)">+</a>
                    </div>
                @endif
                <br>
                <div class="accordion" id="accordion">
                    @foreach($data as $value)
                        <div class="accordion-header" @if($value->status != 1) style="width:93%; background-color:#727272;" @else style="width:93%;" @endif role="button" id="collapse-panel" @if(request()->getHttpHost()=="local.classmild.id") data-toggle="collapse" @endif data-target="#panel-body-{{$value->id}}" aria-expanded="false">
                            <div class="expand">
                                <i class="fas fa-expand"></i>
                            </div>
                            <h4>{{ $value->name}}</h4>
                        </div>
                        <div class="edit">
                            @if(auth()->user()->can('edit channel'))
                                <a href="#" class="btn btn-md btn-primary" onclick="channel_edit('{{route('channel-edit',['id' => $value->id])}}',0,null,1,'{{$value->id}}')"><i class="far fa-edit"></i></a>
                            @endif
                        </div>
                        <div class="delete">
                            @if(auth()->user()->can('delete channel'))
                                <a href="#" onclick='confirmDelete("{{ route("channel-delete",["id" => $value->id]) }}")' class="btn btn-md btn-danger">X</a>
                            @endif
                        </div>
                        <div style="margin-bottom:70px;width:93%;" class="accordion-body accordion-body-1 collapse" id="panel-body-{{$value->id}}" data-parent="#accordion">
                            <div class="accordion" id="accordion2">
                                @if(auth()->user()->can('add channel'))
                                    <div align="right">
                                        <a href="#" class="btn btn-md btn-primary" onclick="channel_add('{{$value->id}}','{{$value->name}}',2)">+</a>
                                    </div><br>
                                @endif
                                @if(count($value->child) > 0)
                                    @foreach($value->child as $value2)
                                    <div class="accordion-header accordion2-header" @if($value2->status != 1) style="width:93%; background-color:#727272;" @else style="width:93%;" @endif role="button" id="collapse-panel" data-toggle="collapse" data-target="#panel-body-{{$value2->id}}" aria-expanded="false">
                                        <div class="expand">
                                            <i class="fas fa-expand"></i>
                                        </div>
                                        <h4>{{$value2->name}}</h4>
                                    </div>
                                    <div class="edit">
                                        @if(auth()->user()->can('edit channel'))
                                            <a href="#" class="btn btn-md btn-primary" onclick="channel_edit('{{route('channel-edit',['id' => $value2->id])}}','{{$value->id}}','{{$value->name}}',2,'{{$value2->id}}')"><i class="far fa-edit"></i></a>
                                        @endif
                                    </div>
                                    <div class="delete">
                                        @if(auth()->user()->can('delete channel'))
                                        <a href="#" onclick='confirmDelete("{{ route("channel-delete",["id" => $value2->id]) }}")' class="btn btn-md btn-danger">X</a>
                                        @endif
                                    </div>
                                    <div style="margin-bottom:70px;width:93%;" class="accordion-body accordion-body-2 collapse" id="panel-body-{{$value2->id}}" data-parent="#accordion2">
                                        <div class="accordion" id="accordion3">
                                            @if(auth()->user()->can('add channel'))
                                                <div align="right">
                                                    <a href="#" class="btn btn-md btn-primary" onclick="channel_add('{{$value2->id}}','{{$value->name}} > {{$value2->name}}',3)">+</a>
                                                </div><br>
                                            @endif
                                            @if(count($value2->child) > 0)
                                                @foreach($value2->child as $value3)
                                                <div class="accordion-header accordion3-header" @if($value3->status != 1) style="width:93%; background-color:#727272;" @else style="width:93%;" @endif role="button" id="collapse-panel" data-toggle="collapse" data-target="#panel-body-{{$value3->id}}" aria-expanded="false">
                                                    <div class="expand">
                                                        <i class="fas fa-expand"></i>
                                                    </div>
                                                    <h4>{{$value3->name}}</h4>
                                                </div>
                                                <div class="edit">
                                                    @if(auth()->user()->can('edit channel'))
                                                        <a href="#" class="btn btn-md btn-primary" onclick="channel_edit('{{route('channel-edit',['id' => $value3->id])}}','{{$value2->id}}','{{$value->name}} > {{$value2->name}}',3,'{{$value3->id}}')"><i class="far fa-edit"></i></a>
                                                    @endif
                                                </div>
                                                <div class="delete">
                                                    @if(auth()->user()->can('delete channel'))
                                                    <a href="#" onclick='confirmDelete("{{ route("channel-delete",["id" => $value3->id]) }}")' class="btn btn-md btn-danger">X</a>
                                                    @endif
                                                </div>
                                                <div class="accordion-body accordion-body-3 collapse" style="width:93%;" id="panel-body-{{$value3->id}}" data-parent="#accordion3">
                                                    <div class="accordion" id="accordion4">
                                                        @if(auth()->user()->can('add channel'))
                                                            <div align="right">
                                                                <a href="#" value="abc" class="btn btn-md btn-primary" onclick="channel_add('{{$value3->id}}','{{$value->name}} > {{$value2->name}} > {{$value3->name}}',4)">+</a>
                                                            </div><br>
                                                        @endif
                                                        @foreach($value3->child as $value4)
                                                        <div class="accordion-header accordion4-header" @if($value4->status != 1) style="width:92%; background-color:#727272;" @else style="width:92%;" @endif role="button" id="collapse-panel" data-toggle="" data-target="#panel-body-{{$value4->id}}" aria-expanded="false">
                                                            <h4>{{$value4->name}}</h4>
                                                        </div>
                                                        <div class="edit">
                                                            @if(auth()->user()->can('edit channel'))
                                                                <a href="#" class="btn btn-md btn-primary" onclick="channel_edit('{{route('channel-edit',['id' => $value4->id])}}','{{$value3->id}}','{{$value->name}} > {{$value2->name}} > {{$value3->name}}',4, '{{$value4->id}}')"><i class="far fa-edit"></i></a>
                                                            @endif
                                                        </div>
                                                        <div class="delete">
                                                            @if(auth()->user()->can('delete channel'))
                                                            <a href="#" onclick='confirmDelete("{{ route("channel-delete",["id" => $value4->id]) }}")' class="btn btn-md btn-danger">X</a>
                                                            @endif
                                                        </div>
                                                        <div class="accordion-body collapse" id="panel-body-{{$value4->id}}" data-parent="#accordion4">
                                                            
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @endforeach
                                            @else
                                                <div style="border-style: dotted;" align="center">TIDAK ADA DATA</div>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <div style="border-style: dotted;" align="center">TIDAK ADA DATA</div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection