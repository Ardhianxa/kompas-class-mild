<?php
 
namespace App\Modules\ConsumerLetter\Controllers;

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\ConsumerLetter\Models\Consumer_Letter;
use Mail;
use Validator;

class ConsumerLetterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function detailPost(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'attachment' => 'file|max:'.(config('constant.MAX_SIZE_IMAGE_UPLOAD')*1000),
        ]);

        if ($validator->fails()) {
            return redirect()->route('consumer-letter-detail',['id' => $id])->with('flash_failed', 'Maksimal Attachment '.config('constant.MAX_SIZE_IMAGE_UPLOAD').' MB');
        }

        $data = Consumer_Letter::Detail($id);

        /*validasi*/
        $validasi = consumerLetterValidation($request,$data->judul);
        if(!$validasi){
            return view('errors.403');
        }

        $data->status = $request->status;
        $data->balasan = $request->reply;
        $destinationPath = "";
        $data->attachment = "";
        if($request->file('attachment') != "")
        {
            $destinationPath = config('constant.ACTUAL_PATH').'public/assets/uploads/img/consumer_letter_attachment/';
            $filename = md5(time()).'.'.$request->file('attachment')->getClientOriginalExtension();
            $upload = $request->file('attachment')->move($destinationPath, $filename);
            $data->attachment = $filename;
        }

        if($data->save()){
            if($request->submit){
                if($data->status==3){
                    $value = array(
                        'balasan' => $data->balasan
                    );
                    $data->email = 'ardhy.anxa@gmail.com';
                    $send = Mail::send('ConsumerLetter::consumer_letter_mail', $value, function ($message) use ($request, $data, $destinationPath)
                    {
                        $message->from(config('constant.MAIL_FROM'), config('constant.APP_NAME'));
                        $message->to($data->email)->subject('CLASS MILD CS');
                        if($data->attachment != ""){
                            $message->attach($destinationPath.$data->attachment);
                        }
                    });
                }
            }
            return redirect()->route('consumer-letter',['status' => 'new'])->with('flash_success', 'Successfully');
        }
        else{
            return redirect()->route('consumer-letter-detail',['id' => $id])->with('flash_failed', 'Internal Server Error');
        }
    }

    public function detail(Request $request, $id)
    {
        $title = 'Detail Suara Konsumen';
        $page = 'ConsumerLetter::detail_consumer_letter';
        $data = Consumer_Letter::Detail($id);

        /*validasi*/
        $validasi = consumerLetterValidation($request,$data->judul);
        if(!$validasi){
            return view('errors.403');
        }
        
        return view($page,compact('title','page','data','id'));
    }

    public function view(Request $request, $id)
    {
        $data = Consumer_Letter::Detail($id);
        return view('ConsumerLetter::view',compact('data'));
    }

    public function index(Request $request, $type, $key = null)
    {
        $title = 'Suara Konsumen';
        $page = 'ConsumerLetter::consumer_letter';

        $new = Consumer_Letter::Data($type,'new',1,$key);
        $in_progress = Consumer_Letter::Data($type,'inprogres',2,$key);
        $done = Consumer_Letter::Data($type,'done',3,$key);

        return view($page,compact('title','page','in_progress','done','key','new','type'));
    }
}
