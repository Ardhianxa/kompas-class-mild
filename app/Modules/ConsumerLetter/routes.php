<?php
Route::group(  
[
	'namespace' => 'App\Modules\ConsumerLetter\Controllers', 
	'prefix' => '', 
	'middleware' => [
						'web',
						'auth',
						'session',
						'before_global',
						'after_global',
						'App\Modules\ConsumerLetter\Middleware\BeforeMiddleware',
						'App\Modules\ConsumerLetter\Middleware\AfterMiddleware'
					]
], 
function ()
{
	Route::get('consumer-letter/detail/{id}', [
		    'as' => 'consumer-letter-detail', 'uses' => 'ConsumerLetterController@detail', 'middleware' => ['permission:edit consumer letter periklanan|edit consumer letter ketersediaan produk|edit consumer letter kualitas produk']
	]);

	Route::get('consumer-letter/view/{id}', [
		'as' => 'consumer-letter-view', 'uses' => 'ConsumerLetterController@view', 'middleware' => ['permission:edit consumer letter periklanan|edit consumer letter ketersediaan produk|edit consumer letter kualitas produk']
]);

	Route::post('consumer-letter/detail/{id}', [
		    'as' => 'post-consumer-letter-detail', 'uses' => 'ConsumerLetterController@detailPost', 'middleware' => ['permission:edit consumer letter periklanan|edit consumer letter ketersediaan produk|edit consumer letter kualitas produk']
	]);

	Route::get('consumer-letter/{status}/{key?}', [
		'as' => 'consumer-letter', 'uses' => 'ConsumerLetterController@index', 'middleware' => ['permission:view consumer letter periklanan|view consumer letter ketersediaan produk|view consumer letter kualitas produk']
	]);
});
?>