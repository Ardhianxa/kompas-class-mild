<?php
if(!function_exists('consumerLetterValidation')){ 
    function consumerLetterValidation($request,$judul)
    {
    	$result = true;
    	if($judul==1 && !$request->user->hasPermissionTo('edit consumer letter periklanan')){
			$result = false;
        }
        elseif($judul==2 && !$request->user->hasPermissionTo('edit consumer letter ketersediaan produk')){
            $result = false;
        }
        elseif($judul==3 && !$request->user->hasPermissionTo('edit consumer letter kualitas produk')){
            $result = false;
        }
        return $result ;
    }
}

if(!function_exists('consumerLetterType')){ 
    function consumerLetterType($type)
    {
    	$result = 0;
    	if(str_replace("-"," ",strtolower($type))=="advertising"){
    		$result = 1;
    	}
    	elseif(str_replace("-"," ",strtolower($type))=="product availability"){
    		$result = 2;
    	}
    	elseif(str_replace("-"," ",strtolower($type))=="product quality"){
    		$result = 3;
    	}
        return $result ;
    }
}

if(!function_exists('consumer_letter_purpose')){ 
	function consumer_letter_purpose($id)
	{
		if($id==1){
			$result = 'Apresiasi';
		}elseif($id==2){
			$result = 'Komplain';
		}elseif($id==3){
			$result = 'Pertanyaan';
		}

		return $result;
	}
}

if(!function_exists('consumer_tracking_id_show')){ 
    function consumer_tracking_id_show($id)
    {
        return substr($id,-5);
    }
}

if(!function_exists('consumerLetterTitle')){ 
    function consumerLetterTitle($title)
    {
    	$result = "";
    	if($title==1){
    		$result = "Periklanan";
    	}
    	elseif($title==2){
    		$result = "Ketersediaan Produk";
    	}
    	elseif($title==3){
    		$result = "Kualitas Produk";
    	}
        return $result ;
    }
}

if(!function_exists('cekJenisEmail')){ 
	function cekJenisEmail($email)
	{
	    $kata_kotor = array("gmail","Gmail");
	    $hasil = 0;
	    $jml_kata = count($kata_kotor);
	    for ($i=0;$i<$jml_kata;$i++)
	    {
	        if (stristr($email,$kata_kotor[$i]))
	        { 
	            $hasil=1; 
	        }
	    } 

	    return $hasil;
	}
}

if(!function_exists('bodyMailReplyConsumerLetter'))
{ 
	function bodyMailReplyConsumerLetter($email,$url_assets,$data)
	{
	    $logo = $url_assets.'user/img/logo.png';
	    $top = $url_assets.'user/img/top.jpg';
	    $bgg = $url_assets.'user/img/bgg.jpg';

	    $body = "
	            <!DOCTYPE html>
	            <html>
	            <head>
	            <style type='text/css'>
	            body {
	            font-size: 0.8rem !important;
	            color: #000 !important;
	            }

	            .container, .container-fluid {
	            padding-left: 1.5rem;
	            padding-right: 1.5rem;
	            }

	            .first-td{
	                width: 24rem;
	                padding-right: 5rem;
	            }

	            </style>
	            <head>  
	                <body style='height:1100px;width:1000px; font-family:Arial, Helvetica, sans-serif; font-size:1em; line-height:1.5em; background:url($bgg) left bottom no-repeat';background-size: 100% 100%;>                
	                    <div style='padding-left:10px;width:100%; background: url($top) no-repeat; height:2000px; margin-bottom:25px;'>
	                        <img src='$logo' style='padding-top:10px;float:left;' width='200px'><br><br><br><br><br><br>
	                        <div>
	                            ".$data."
	                        </div>
	                    </div>
	                    <br><br><br><br>
	                </body>
	            </head>
	            </html>  
	        ";

	    return $body;
	}
}

