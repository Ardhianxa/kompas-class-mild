@extends('layouts.app') 

@section('before_sumit')
<script>
$('#myform').submit(function() {
	var val = $("input[type=submit][clicked=true]").val();
	var status = document.getElementById('status').value;
	var status_data = document.getElementById('status-data').value;

	if(val=="Submit"){
		if(status_data==1 && status==1){
			notif("Tentukan Status Pesan","info");
			return false;
		}
		else if(status_data==2 && status != 3){
			notif("Tentukan Status Pesan","info");
			return false;
		}
		return true;
	}
	return true;
});

$("form input[type=submit]").click(function() {
	$("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
	$(this).attr("clicked", "true");
});
</script>
@endsection

@section('content')
<div class="col-12 col-md-12 col-lg-12">
	<form method="post" id="myform" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('post-consumer-letter-detail', ['id' => $id]) }}">
	<input type="hidden" name="status-data" id="status-data" value="{{$data->status}}">
	<div class="card">
    	<div class="card-body">
    		@csrf
    		<div class="row">                               
		        <div class="form-group col-md-4 col-12">
		          	<label>Pesan</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ consumerLetterTitle($data->judul) }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
		        <div class="form-group col-md-4 col-12">
		          	<label>Pernyataan</label>
		          	<input type="text" class="form-control" name="first_name" value="{{consumer_letter_purpose($data->tujuan_pernyataan)}}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
		        <div class="form-group col-md-4 col-12">
		          	<label>Email</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->email }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
	      	</div>
	      	<div class="row">                               
		        <div class="form-group col-md-4 col-12">
		          	<label>Name</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->nama }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
		        <div class="form-group col-md-4 col-12">
		          	<label>Telepon</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->telepon }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
				</div>
				<div class="form-group col-md-4 col-12">
		          	<label>Tracking ID</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ consumer_tracking_id_show($data->tracking_id) }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
				</div>
	      	</div>
			<div class="row">                               
				<div class="form-group col-md-4 col-12">
		          	<label>Nama Produk</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->nama_produk }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
		        <div class="form-group col-md-4 col-12">
		          	<label>Lokasi Pembelian</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->lokasi_pembelian }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
				</div>
				<div class="form-group col-md-4 col-12">
		          	<label>Provinsi</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->provinsi }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
			  </div>
			<div class="row">
				<div class="form-group col-md-4 col-12">
		          	<label>Kota</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->kota }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
				</div>
				<div class="form-group col-md-4 col-12">
		          	<label>Kecamatan</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->kecamatan }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
				</div>
				<div class="form-group col-md-4 col-12">
		          	<label>Postcode</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->kode_pos }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
			</div>
			<div class="row">                               
			  	<div class="form-group col-md-12 col-12">
		          	<label>Alamat</label>
		          	<textarea style="height:500px;" class="form-control" name="first_name" readonly>{{ $data->alamat }}</textarea>
				</div>
			</div>
			<div class="row">                               
		        <div class="form-group col-md-12 col-12">
					<label>Location in Map</label>
					<div id="map_canvas" style="width: 100%; height: 600px"></div>
				</div>
			</div>
	      	@if($data->judul==3)
	      	<div class="row">                               
		        <div class="form-group col-md-6 col-12">
		          	<label>Production Date</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ date_without_hours($data->tanggal_produksi) }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
		        <div class="form-group col-md-6 col-12">
		          	<label>Production Code</label>
		          	<input type="text" class="form-control" name="first_name" value="{{ $data->kode_produksi }}" required="" readonly>
		          	<div class="invalid-feedback">
		            	Please fill in the first name
		          	</div>
		        </div>
	      	</div>
	      	@endif
	      	<div class="row">                               
		        <div class="form-group col-md-12 col-12">
		          	<label>Message</label>
		          	<textarea style="height:500px;" class="form-control" name="first_name" readonly>{{ $data->pesan }}</textarea>
		        </div>
	      	</div>
	      	<div class="row">                               
		        <div class="form-group col-md-12 col-12">
		          	<label>Consumer Attachment</label>
		          	@if($data->user_attachment != '')
                  		<br>
                  		<a href="{{ config('constant.ASSETS_URL').'uploads/img/consumer_letter_attachment/'.$data->user_attachment }}" target="_blank">{{ config('constant.ASSETS_URL').'uploads/img/consumer_letter_attachment/'.$data->user_attachment }}</a>
                  	@endif
		        </div>
	      	</div>
	      	<div class="row">                               
		        <div class="form-group col-md-12 col-12">
		          	<label>Status</label>
		          	<select class="form-control" name="status" id="status">
		          		<option @if($data->status==1) selected="selected" @endif value="1">New</option>
		          		<option @if($data->status==2) selected="selected" @endif value="2">In Progres</option>
		          		<option @if($data->status==3) selected="selected" @endif value="3">Done</option>
		          	</select>
		        </div>
	      	</div>
	      	<div class="row"> 
		      	<div class="form-group col-md-12 col-12">
                  	<label>Reply</label>
                  	<textarea name="reply" required="required" class="summernote-simple">{{ $data->balasan }}</textarea>
                </div>
        	</div>
        	<div class="row"> 
		      	<div class="form-group col-md-12 col-12">
                  	<label>Attachment</label>
					  	<input type="file" name="attachment" class="form-control"><br>
						<div class="notif-upload">
							Max Size : {{config('constant.MAX_SIZE_IMAGE_UPLOAD')}} MB
						</div>
						@if($data->attachment != '')
							<br>
							<a href="{{ config('constant.ASSETS_URL').'uploads/img/consumer_letter_attachment/'.$data->attachment }}" target="_blank">{{ config('constant.ASSETS_URL').'backend/consumer_letter_attachment/'.$data->attachment }}</a>
						@endif
                </div>
        	</div>
	      	<div class="row">                               
		        <div class="form-group col-md-12 col-12 text-right">
					<input type="submit" class="btn btn-success" name="save" value="Save">
					<input type="submit" class="btn btn-primary" name="submit" value="Submit">
		        </div>
	      	</div>
	    </div>
	</div>
	</form>
</div>
@endsection