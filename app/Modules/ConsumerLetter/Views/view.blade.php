<div class="row">                               
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Pesan</label><br>
        {{ consumerLetterTitle($data->judul) }}
    </div>
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Pernyataan</label><br>
        {{consumer_letter_purpose($data->tujuan_pernyataan)}}
    </div>
</div>
<div class="row">                               
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Email</label><br>
        {{ $data->email }}
    </div>
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Name</label><br>
        {{ $data->nama }}
    </div>
</div>
<div class="row">                               
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Telepon</label><br>
        {{ $data->telepon }}
    </div>
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Tracking ID</label><br>
        {{ consumer_tracking_id_show($data->tracking_id) }}
    </div>
</div>
<div class="row">                               
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Nama Produk</label><br>
        {{ $data->nama_produk }}
    </div>
    <div class="form-group col-sm-12 col-md-12 col-lg-6">
        <label>Lokasi Pembelian</label><br>
        {{ $data->alamat }}, {{ $data->kecamatan }}, {{ $data->kota }} <br>
        {{ $data->provinsi }} <br>
        {{ $data->kode_pos }}
    </div>
</div>
@if($data->judul==3)
    <div class="row">                               
        <div class="form-group col-md-6 col-6">
            <label>Production Date</label>
            <input type="text" class="form-control" name="first_name" value="{{ date_without_hours($data->tanggal_produksi) }}" required="" readonly>
            <div class="invalid-feedback">
                Please fill in the first name
            </div>
        </div>
        <div class="form-group col-md-6 col-6">
            <label>Production Code</label>
            <input type="text" class="form-control" name="first_name" value="{{ $data->kode_produksi }}" required="" readonly>
            <div class="invalid-feedback">
                Please fill in the first name
            </div>
        </div>
    </div>
@endif
<div class="row">                               
    <div class="form-group col-sm-12 col-md-12 col-lg-12">
        <label>Message</label><br>
        {{ $data->pesan }}
    </div>
</div>

@if($data->user_attachment != '')
    <div class="row">                               
        <div class="form-group col-sm-12 col-md-12 col-lg-12">
            <label>Attachment</label><br>
            <div style="white-space: pre-wrap; /* CSS3 */    
            white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
            white-space: -pre-wrap; /* Opera 4-6 */    
            white-space: -o-pre-wrap; /* Opera 7 */    
            word-wrap: break-word; /* Internet Explorer 5.5+ */">
            <a href="{{ config('constant.ASSETS_URL').'backend/consumer_letter_attachment/'.$data->user_attachment }}" target="_blank">
            {{ config('constant.ASSETS_URL').'backend/consumer_letter_attachment/'.$data->user_attachment }}</a>
            </div>
        </div>
    </div>
@endif
