@extends('layouts.app')     

@section('content')
<div class="section-body">
<div class="row">
	<div class="col-12 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-body">
                <ul class="nav nav-pills" id="myTab3" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link @if($type=='new') active @endif" id="tab-new" data-toggle="tab" href="#new" role="tab" aria-controls="new" aria-selected="true">New</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link @if($type=='inprogres') active @endif" id="tab-inprogres" data-toggle="tab" href="#inprogres" role="tab" aria-controls="inprogres" aria-selected="false">In Porgress</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link @if($type=='done') active @endif" id="tab-done" data-toggle="tab" href="#done" role="tab" aria-controls="done" aria-selected="false">Done</a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent2">
                  	<div class="tab-pane fade show @if($type=='new') active @endif" id="new" role="tabpanel" aria-labelledby="tab-new">
                      	<div class="card-body p-0 box-table-search">
			            	<div class="col-12 col-md-12 col-lg-3">
			                	<div class="input-group mb-3">
			                        <input type="text" class="form-control" id="search-new" placeholder="Search" aria-label="" value="@if($type=='new'){{ $key }}@endif">
			                        <div class="input-group-append">
			                          <a onclick="redirect('{{ config('constant.APP_URL') }}/consumer-letter/new/'+document.getElementById('search-new').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
			                        </div>
		                      	</div>
			            	</div> 
			            </div>
                        <div class="table-responsive">
		                  <table class="table table-striped table-md">
		                    <tr>
		                      <th width="5%">No</th>
		                      <th>Name</th>
							  <th>Email</th>
							  <th>Pernyataan</th>
		                      <th>Pesan</th>
		                      <th>Created Date</th>
		                      <th width="10%"></th>
		                    </tr>
		                    @php
		                    $no=1;
		                    @endphp
		                    @if(count($new) > 0)
		                    @foreach($new as $new_value)
		                    <tr>
		                      <td>{{ ($no+($new->perPage()*($new->currentPage()-1))) }}</td>
		                      <td>{{ $new_value->nama }}</td>
							  <td>{{ $new_value->email }}</td>
							  <td>{{consumer_letter_purpose($new_value->tujuan_pernyataan)}}</td>
		                      <td>
		                      	{{ consumerLetterTitle($new_value->judul) }}
		                      </td>
		                      <td>{{ date_with_hours($new_value->created_at) }}</td>
		                      <td>
		                      	@if(
									  (auth()->user()->can('edit consumer letter periklanan') && $new_value->judul==1) || 
									  (auth()->user()->can('edit consumer letter ketersediaan produk') && $new_value->judul==2) || 
									  (auth()->user()->can('edit consumer letter kualitas produk') && $new_value->judul==3)
									)
									<a class="btn btn-icon btn-primary" href='#' onclick="detail_consumer_letter('{{$new_value->id}}')"><i class="fa fa-book-reader"></i></a>
		                      		<a class="btn btn-icon btn-primary" href='{{ route("consumer-letter-detail",["id" => $new_value->id]) }}'><i class="far fa-edit"></i></a>
		                      	@endif
		                      </td>
		                    </tr>
		                    @php
		                    $no++;
		                    @endphp
		                    @endforeach
		                    @else
		                    	<tr>
		                    		<td colspan="7"><div align="center">-No Data-</div></td>
		                    	</tr>
		                    @endif
		                  </table>
		                </div>
                        <div class="card-footer text-right">
			                <nav class="d-inline-block">
			                  {{ $new->links() }}
			                </nav>
		              	</div>
                  	</div>
                  	<div class="tab-pane fade show @if($type=='inprogres') active @endif" id="inprogres" role="tabpanel" aria-labelledby="tab-inprogres">
                    <div class="card-body p-0 box-table-search">
			            <div class="col-12 col-md-12 col-lg-3">
			                	<div class="input-group mb-3">
			                        <input type="text" class="form-control" id="search-inprogres" placeholder="Search" aria-label="" value="@if($type=='inprogres'){{ $key }}@endif">
			                        <div class="input-group-append">
			                          <a onclick="redirect('{{ config('constant.APP_URL') }}consumer-letter/inprogres/'+document.getElementById('search-inprogres').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
			                        </div>
		                      	</div>
			            	</div>
			            </div>
                        <div class="table-responsive">
		                  <table class="table table-striped table-md">
		                    <tr>
		                      <th width="5%">No</th>
							  <th>Name</th>
							  <th>Email</th>
							  <th>Pernyataan</th>
		                      <th>Pesan</th>
		                      <th>Created Date</th>
		                      <th width="10%">Action</th>
		                    </tr>
		                    @php
		                    $no=1;
		                    @endphp
		                    @if(count($in_progress) > 0)
		                    @foreach($in_progress as $in_progress_value)
		                    <tr>
		                      <td>{{ ($no+($in_progress->perPage()*($in_progress->currentPage()-1))) }}</td>
		                      <td>{{ $in_progress_value->nama }}</td>
							  <td>{{ $in_progress_value->email }}</td>
							  <td>{{consumer_letter_purpose($in_progress_value->tujuan_pernyataan)}}</td>
		                      <td>
		                      	{{ consumerLetterTitle($in_progress_value->judul) }}
		                      </td>
		                      <td>{{ date_with_hours($in_progress_value->created_at) }}</td>
		                      <td>
							  	@if(
									  (auth()->user()->can('edit consumer letter periklanan') && $in_progress_value->judul==1) || 
									  (auth()->user()->can('edit consumer letter ketersediaan produk') && $in_progress_value->judul==2) || 
									  (auth()->user()->can('edit consumer letter kualitas produk') && $in_progress_value->judul==3)
									)
									<a class="btn btn-icon btn-primary" href='#' onclick="detail_consumer_letter('{{$in_progress_value->id}}')"><i class="fa fa-book-reader"></i></a>
									<a class="btn btn-icon btn-primary" href='{{ route("consumer-letter-detail",["id" => $in_progress_value->id]) }}'><i class="far fa-edit"></i></a>
		                      	@endif
		                      </td>
		                    </tr>
		                    @php
		                    $no++;
		                    @endphp
		                    @endforeach
		                    @else
		                    	<tr>
		                    		<td colspan="7"><div align="center">-No Data-</div></td>
		                    	</tr>
		                    @endif
		                  </table>
		                </div>
                        <div class="card-footer text-right">
			                <nav class="d-inline-block">
			                  {{ $in_progress->links() }}
			                </nav>
		              	</div>
                  	</div>
                  	<div class="tab-pane fade show @if($type=='done') active @endif" id="done" role="tabpanel" aria-labelledby="tab-done">
                    <div class="card-body p-0 box-table-search">
			            <div class="col-12 col-md-12 col-lg-3">
			                	<div class="input-group mb-3">
			                        <input type="text" class="form-control" id="search-done" placeholder="Search" aria-label="" value="@if($type=='done'){{ $key }}@endif">
			                        <div class="input-group-append">
			                          <a onclick="redirect('{{ config('constant.APP_URL') }}consumer-letter/done/'+document.getElementById('search-done').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
			                        </div>
		                      	</div>
			            	</div>
			            </div>
                        <div class="table-responsive">
		                  <table class="table table-striped table-md">
		                    <tr>
		                      <th width="5%">No</th>
							  <th>Name</th>
							  <th>Email</th>
							  <th>Pernyataan</th>
		                      <th>Pesan</th>
		                      <th>Created Date</th>
		                      <th width="10%">Action</th>
		                    </tr>
		                    @php
		                    $no=1;
		                    @endphp
		                    @if(count($done) > 0)
		                    @foreach($done as $done_value)
		                    <tr>
		                      <td>{{ ($no+($done->perPage()*($done->currentPage()-1))) }}</td>
		                      <td>{{ $done_value->nama }}</td>
							  <td>{{ $done_value->email }}</td>
							  <td>{{consumer_letter_purpose($done_value->tujuan_pernyataan)}}</td>
		                      <td>
		                      	{{ consumerLetterTitle($done_value->judul) }}
		                      </td>
		                      <td>{{ date_with_hours($done_value->created_at) }}</td>
		                      <td>
							  		@if(
									  (auth()->user()->can('edit consumer letter periklanan') && $done_value->judul==1) || 
									  (auth()->user()->can('edit consumer letter ketersediaan produk') && $done_value->judul==2) || 
									  (auth()->user()->can('edit consumer letter kualitas produk') && $done_value->judul==3)
									)
										<a class="btn btn-icon btn-primary" href='#' onclick="detail_consumer_letter('{{$done_value->id}}')"><i class="fa fa-book-reader"></i></a>
										<a class="btn btn-icon btn-primary" href='{{ route("consumer-letter-detail",["id" => $done_value->id]) }}'><i class="far fa-edit"></i></a>
		                      	@endif
		                      </td>
		                    </tr>
		                    @php
		                    $no++;
		                    @endphp
		                    @endforeach
		                    @else
		                    	<tr>
		                    		<td colspan="7"><div align="center">-No Data-</div></td>
		                    	</tr>
		                    @endif
		                  </table>
		                </div>
                        <div class="card-footer text-right">
			                <nav class="d-inline-block">
			                  {{ $done->links() }}
			                </nav>
		              	</div>
                  	</div>
                </div>
             </div>
        </div>
	</div>
</div>
</div>

<script>
function detail_consumer_letter(id)
{
	var global_app_url = "{{config('constant.APP_URL')}}";
	$.ajax({
        url: global_app_url+"/consumer-letter/view/"+id,
        type: "get",
        data: {
            id : id
        },
        beforeSend: function () {
        
        },
        error: function() {
        
        },
        success: function (html) {
			$('.modal-consumer-letter-detail .modal-body').html('');
			$('.modal-consumer-letter-detail .modal-body').html(html);
            $('.modal-category-add').modal('show');
        }
	});
	
	$('.modal-consumer-letter-detail').modal('show');
}
</script>
@endsection

@section('modal-data')
<div class="modal fade modal-consumer-letter-detail" id="ads" tabindex="-1" role="dialog" aria-labelledby="modal-category" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="z-index:10000">
            <div class="modal-header">
                <h4 class="modal-title" id="" style="padding-bottom:20px;"><div class="category-title"></div></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>
@endsection