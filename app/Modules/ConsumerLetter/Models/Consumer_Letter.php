<?php

namespace App\Modules\ConsumerLetter\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Consumer_Letter extends Model
{
    protected $table = 'consumer_letter';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nama',
		'email',
        'alamat',
        'kota',
        'telepon',
        'judul',
        'pesan',
        'balasan',
        'status'
    ];

    protected $hidden = [
    ];

    public static function Detail($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "consumer-letter_detail_".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Detail_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Detail_data($id);
        }

        return $data;
    }

    public static function Detail_data($id)
    {
        return Consumer_Letter::select('tracking_id','lat','lon','id','nama','email','alamat','kota','telepon','judul','pesan','created_at','status','attachment','balasan','user_attachment','provinsi','kecamatan','kelurahan','kode_pos','tujuan_pernyataan','nama_produk','lokasi_pembelian','tanggal_produksi','kode_produksi')->where('id',$id)->first();
    }

    

    public static function Data($type,$type2,$status,$key)
    {
        if(config('constant.CACHE')){
            $cache_name = "consumer-letter_data_".$type."_".$type2."_".$status.'_'.$key;
            if(!Cache::has($cache_name)) {
                $save = Cache::put($cache_name, Self::Data_data($type,$type2,$status,$key), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data($type,$type2,$status,$key);
        }

        return $data;
    }

    public static function Data_data($type,$type2,$status,$key)
    {
        $data = Self::select('id','judul','nama','email','alamat','kota','telepon','judul','pesan','created_at','tujuan_pernyataan')->where('status',$status);

        if(strtolower($type)==$type2 && $key != null){
            $data->whereRaw("
            (
                email LIKE '%".$key."%' or 
                kota LIKE '%".$key."%' or 
                nama LIKE '%".$key."%' or 
                alamat LIKE '%".$key."%' or 
                telepon LIKE '%".$key."%' or 
                judul LIKE '%".$key."%' or 
                pesan LIKE '%".$key."%' or
                tracking_id LIKE '%".$key."'
            )
            ");
        }

        $data->orderBy('created_at','desc');

        return $data->paginate(config('constant.LIMIT_DATA_TABLE'));
    }
}
