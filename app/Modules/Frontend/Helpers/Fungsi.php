<?php
if(!function_exists('age')){ 
    function age($tanggal_lahir){
        $birthDate = new DateTime($tanggal_lahir);
        $today = new DateTime("today");
        if ($birthDate > $today) { 
            exit("0 tahun 0 bulan 0 hari");
        }
        $y = $today->diff($birthDate)->y;
        $m = $today->diff($birthDate)->m;
        $d = $today->diff($birthDate)->d;
        
        return $y;
    }
}


if(!function_exists('consumerLetterTitle')){ 
    function consumerLetterTitle($title)
    {
    	$result = "";
    	if($title==1){
    		$result = "Advertising";
    	}
    	elseif($title==2){
    		$result = "Product Availability";
    	}
    	elseif($title==3){
    		$result = "Product Quality";
    	}
        return $result ;
    }
}

if(!function_exists('consumerLetterStatus')){ 
    function consumerLetterStatus($id)
    {
        $result = "";
        if($id==1){
            $result = "Received";
        }
        elseif($id==2){
            $result = "Inprogress";
        }
        elseif($id==3){
            $result = "Done";
        }

        return $result;
    }
}

if(!function_exists('consumer_tracking_id_show')){ 
    function consumer_tracking_id_show($id)
    {
        return substr($id,-5);
    }
}

if(!function_exists('consumer_letter_purpose')){ 
	function consumer_letter_purpose($id)
	{
		if($id==1){
			$result = 'Appreciation';
		}elseif($id==2){
			$result = 'Complain';
		}elseif($id==3){
			$result = 'Question';
		}

		return $result;
	}
}
?>