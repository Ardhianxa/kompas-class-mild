<?php
namespace App\Modules\Frontend\Models; 

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class Frontend extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id',
		'name',
        'status'
    ];

    protected $hidden = [
    ];

    public static function cekUsia()
    {
        return Session::get('cekUsia');
    }

    public static function consumer_letter_data($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "clas-mild-tracking-".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::consumer_letter_data_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::consumer_letter_data_data($id);
        }

        return $data;
    }

    public static function consumer_letter_data_data($id)
    {
        if(!empty($id)){
            return DB::table('consumer_letter')->where('tracking_id','LIKE','%'.$id)->paginate(10);
        }
        else{
            return DB::table('consumer_letter')->paginate(10);
        }
    }

    public static function story()
    {
        if(config('constant.CACHE')){
            $cache_name = "clas-mild-story";
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::story_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::story_data();
        }

        return $data;
    }

    public static function story_data()
    {
        $data = array();
        $data['id'] = 1;
        $data['keywords'] = 'clasmild, rokok, indonesia, tembakau, story';
        $data['title'] = 'Clas Mild - Rokok Ternama di Indonesia - Clas Mild Story';
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        
        return $data;
    }

    public static function campaign_story()
    {
        if(config('constant.CACHE')){
            $cache_name = "campaign-story";
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::campaign_story_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::campaign_story_data();
        }

        return $data;
    }

    public static function provinsi()
    {
        if(config('constant.CACHE')){
            $cache_name = "provinsi";
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::provinsi_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::provinsi_data();
        }

        return $data;
    }

    public static function provinsi_data()
    {
        return DB::table('regional')->select('provinsi')->distinct()->get();
    }

    public static function kabupaten($provinsi)
    {
        if(config('constant.CACHE')){
            $cache_name = "kabupaten_".$provinsi;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::kabupaten_data($provinsi), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::kabupaten_data($provinsi);
        }

        return $data;
    }

    public static function kabupaten_data($provinsi)
    {
        return DB::table('regional')->select('kabupaten')->distinct()->where('provinsi',$provinsi)->get();
    }

    public static function kecamatan($provinsi,$kabupaten)
    {
        if(config('constant.CACHE')){
            $cache_name = "kecamatan_".$provinsi."_".$kabupaten;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::kecamatan_data($provinsi,$kabupaten), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::kecamatan_data($provinsi,$kabupaten);
        }

        return $data;
    }

    public static function kecamatan_data($provinsi,$kabupaten)
    {
        return DB::table('regional')->select('kecamatan')->distinct()->where('provinsi',$provinsi)->where('kabupaten',$kabupaten)->get();
    }

    public static function kodepos($provinsi,$kabupaten,$kecamatan)
    {
        if(config('constant.CACHE')){
            $cache_name = "kecamatan_".$provinsi."_".$kabupaten."_".$kecamatan;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::kodepos_data($provinsi,$kabupaten,$kecamatan), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::kodepos_data($provinsi,$kabupaten,$kecamatan);
        }

        return $data;
    }

    public static function kodepos_data($provinsi,$kabupaten,$kecamatan)
    {
        return DB::table('regional')->select('kodepos')->where('provinsi',$provinsi)->where('kabupaten',$kabupaten)->where('kecamatan',$kecamatan)->get();
    }

    public static function campaign_story_data()
    {
        $result = array();
        $year = array();
        $year['2003'] = array();
        $year['2004'] = array();
        $year['2005'] = array();
        $year['2006'] = array();

        $data = array();
        $data['id'] = 1;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2003'][] = $data;

        $data = array();
        $data['id'] = 2;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2003'][] = $data;

        $data = array();
        $data['id'] = 3;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2003'][] = $data;

        $data = array();
        $data['id'] = 4;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2003'][] = $data;

        $data = array();
        $data['id'] = 1;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2004'][] = $data;
        
        $data = array();
        $data['id'] = 2;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2004'][] = $data;

        $data = array();
        $data['id'] = 3;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2004'][] = $data;

        $data = array();
        $data['id'] = 4;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2004'][] = $data;

        $data = array();
        $data['id'] = 1;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2005'][] = $data;
        
        $data = array();
        $data['id'] = 2;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2005'][] = $data;

        $data = array();
        $data['id'] = 3;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2005'][] = $data;

        $data = array();
        $data['id'] = 4;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2005'][] = $data;

        $data = array();
        $data['id'] = 1;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2006'][] = $data;
        
        $data = array();
        $data['id'] = 2;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2006'][] = $data;

        $data = array();
        $data['id'] = 3;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2006'][] = $data;

        $data = array();
        $data['id'] = 4;
        $data['text'] = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.';
        $data['image'] = 'vid.jpg';
        $data['video'] = '0TdC6RfITz8';
        $result['2006'][] = $data;

        return $result;
    }
}
