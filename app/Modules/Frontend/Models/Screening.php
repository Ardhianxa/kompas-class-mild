<?php
namespace App\Modules\Frontend\Models; 

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class Screening extends Model
{
    protected $table = 'screening_frontend';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];
}
