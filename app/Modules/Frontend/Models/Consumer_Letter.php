<?php

namespace App\Modules\Frontend\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Consumer_Letter extends Model
{
    protected $table = 'consumer_letter';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nama',
		'email',
        'alamat',
        'kota',
        'telepon',
        'judul',
        'pesan',
        'balasan',
        'status'
    ];

    protected $hidden = [
    ];
}
