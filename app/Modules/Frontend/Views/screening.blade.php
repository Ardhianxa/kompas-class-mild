@extends('Frontend::global.template')  

@section('content')
<section class="bg_konfirmasi">
    <form method="POST" action="{{route('screening-post')}}">
        @csrf
        <div class="container-fluid">
            <div class="container">
                <div class="row konfirmasibox mb-2">
                    <div class="col-sm txshadow text-center">
                        <p>
                            <h3>INFORMASI DALAM WEBSITE INI DITUJUKAN UNTUK PEROKOK BERUSIA 18 TAHUN ATAU LEBIH DAN TINGGAL DI WILAYAH INDONESIA</h3>
                        </p>
                        <p> Isi Tanggal Lahir Dulu Ya </p>
                    </div>
                </div>
            <div class="container">
                <div class="d-flex justify-content-center form-group">
                    <div class="m1_input">
                        <select name="tanggal" required="required" class="form-control form-control-lg input-form" id="date01">
                            <option value="">-</value>
                            <?php
                            // array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
                            foreach ( range( 1, 31 ) as $number ) {
                                echo '<option value=' . $number . '>' . $number . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="m1_input">
                        <select name="bulan" required="required" class="form-control form-control-lg" id="date02">
                            <option value="">-</value>
                            <?php
                            $monthArray = range( 1, 12 );
                            ?>
                            <?php
                            foreach ( $monthArray as $month ) {
                                $monthPadding = str_pad( $month, 2, "0", STR_PAD_LEFT );
                                $fdate = date( "F", strtotime( "2015-$monthPadding-01" ) );
                                $vdate = date( "m", strtotime( "2015-$monthPadding-01" ) );
                                echo '<option value=' . $vdate . '>' . $fdate . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="m1_input">
                        <select name="tahun" required="required" class="form-control form-control-lg" id="date02">
                            <option value="">-</value>
                            <?php
                            for ( $i = 1950; $i <= date( 'Y' ); $i++ ) {
                                echo '<option value=' . $i . '>' . $i . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row justify-content-md-center konfirmasibox text-center mb-2">
                    <div class="col-md-auto">
                        <p class="txshadow">Apakah Anda Merokok?</p>
                        <div class="row">
                            <div class="col-md-auto">
                                <div class="custom-control custom-radio">
                                    <input type="radio" onclick="screening_pilih_rokok(1)" required="required" class="custom-control-input" id="rad01" name="anda_merokok" value="1">
                                    <label class="custom-control-label" for="rad01">YA</label>
                                </div>
                            </div>
                            <div class="col-md-auto">
                                <div class="custom-control custom-radio">
                                    <input type="radio" onclick="screening_pilih_rokok(0)" required="required" class="custom-control-input" id="rad02" name="anda_merokok" value="0">
                                    <label class="custom-control-label" for="rad02">TIDAK</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center konfirmasibox text-center mb-2">
                    <div class="col-md-auto list-rokok">
                        <p class="txshadow">Merek Rokok Yang Dipakai?</p>
                        <div class="w-100"></div>
                        <div class="col-md-auto">
                            <div class="form-group">
                                <select class="form-control form-control-lg" id="sel1" name="merek_rokok">
                                <option>Clas Mild</option>
                                <option>Clas Mild Silver</option>
                                <option>A Mild</option>
                                <option>Dunhill Mild</option>
                                <option>Dunhill Filter</option>
                                <option>Magnum Mild</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm text-center">
                        <input type="submit" class="btn btn-primary btn-lg" value="KONFIRMASI"></input>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection

@section('js_script')
<script>
$( document ).ready(function() {
    screening_pilih_rokok(0);
});
</script>
@endsection