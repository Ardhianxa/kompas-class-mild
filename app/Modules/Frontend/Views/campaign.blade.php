@extends('Frontend::global.template')     

@section('content')
<section class="bgslide-01">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="owl-carousel owl-theme owl-loaded">
          <div class="item" data-hash="01" style="width:100%;">
            <div class="vid-thumb"> <a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/videoplayback.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2003.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="02">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/YIG-RaveNew.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2005.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="03">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//dc694.4shared.com/img/ydQ36_JCea/f62b723f/dlink__2Fdownload_2FydQ36_5FJCea_3Fsbsr_3Db03cbc0cd1552d2e7f727011e1bd8756a63_26bip_3DMTE4LjEzNy4yMTcuMTQy_26lgfp_3D66_26dsid_3D-GvWfA7l.cbced19702e0d6746cb6c8de903e844e_26bip_3DMTE4LjEzNy4yMTcuMTQy_26bip_3DMTE4LjEzNy4yMTcuMTQy/preview.mp4?cuid=1443571904&cupa=7209ada200ed2eb1c96042b021c9de2c"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2006.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="04">
            <div class="vid-thumb"> <a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/TLDM-RockConcertNew.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2008.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="05">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/09/AN-SenyumNew.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2013.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="06">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clasmild_30Rev27092016....mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2016.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="07">
            <div class="vid-thumb"> <a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2018/08/CLASSMILD-6-15-Dtk-Versi-Kerang.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2019.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
          <div class="item" data-hash="08">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clas-Mild_The-Journey_30s.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2020.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
			<div class="item" data-hash="09">
            <div class="vid-thumb"><a href="#" class="js-video-button" data-channel="video" data-video-url="//clas-mild.com/wp-content/uploads/2017/08/Clas-Mild_The-Journey_30s.mp4"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/2020.jpg" class="img-fluid"></a> </div>
            <div class="vid-desc"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui. </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tahuntx"> 
	  <a href="#01" id="thn" class="active thn01">
		  <span>2003 - 2004</span>&nbsp;
		  <span>2005</span>&nbsp;
		  <span>2006-2007</span>&nbsp;
	  </a>
	  <a href="#04" id="thn" class="thn02">
		  <span>2008 -2012</span>&nbsp;
		  <span>2013-2015</span>&nbsp;
		  <span>2016</span>&nbsp;
	  </a>
	  <a href="#07" id="thn" class="thn03">
		  <span>2019</span>&nbsp;
		  <span>2020</span>&nbsp;
		  <span>2021</span>&nbsp;
	  </a>
	</div>
</section>
@endsection