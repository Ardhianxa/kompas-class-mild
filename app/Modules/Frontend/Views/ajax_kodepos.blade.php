<label for="tx04">Kode Pos</label>
<select class="form-control" id="tx04" name="kodepos" required="required">
    <option value="">-Pilih Kodepos-</option>
    @foreach($kodepos as $kodepos_data)
        <option value="{{$kodepos_data->kodepos}}">{{$kodepos_data->kodepos}}</option>
    @endforeach
</select>