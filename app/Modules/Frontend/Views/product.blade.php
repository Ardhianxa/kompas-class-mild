@extends('Frontend::global.template')  

@section('content')
<section class="bg_produk">
  <div class="container-fluid">
    <div class="container text-center">
      <div class="row">
        <div class="col-sm">
          <h2 class="txhidden">PRODUK KAMI</h2>
          <p class="m-5 txhidden">Terbuat dari bahan tembakau terbaik, Clas Mild mengeluarkan 3 varian SKU Clas Mild 12, Clas Mild Silver dan Clas Mild 16, untuk konsumen yang mengutamakan kualitas dan cita rasa otentik.</p>
        </div>
      </div>
    </div>
    <div class="container" id="produk-rokok">
      <div class="row">
        <div class="col-sm" id="cm12box">
          <div class="flexbox">
            <div class="fleximg"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/CM12.png" alt="" class="img-fluid">
              <div class="plus"> <a href="#!" id="plus01"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/plus.png" alt=""></a> </div>
              <img src="{{ config('constant.ASSETS_URL') }}frontend/images/close.png" alt="" class="tutup01"> </div>
            <div class="flxtxt">
              <h4>CLAS MILD 12</h4>
              <p class="desc-rokok">aliquip ex ea commodo consequat.<br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br>
                cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
        </div>
        <div class="col-sm" id="cm16box">
          <div class="flexbox">
            <div class="fleximg"> <img src="{{ config('constant.ASSETS_URL') }}frontend/images/CM16.png" alt="" class="img-fluid">
              <div class="plus"> <a href="#!" id="plus03"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/plus.png" alt=""></a> </div>
              <img src="{{ config('constant.ASSETS_URL') }}frontend/images/close.png" alt="" class="tutup03"> </div>
            <div class="flxtxt">
              <h4>CLAS MILD 16</h4>
              <p class="desc-rokok">aliquip ex ea commodo consequat.<br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br>
                cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
        </div>
        <div class="col-sm" id="cmsbox">
          <div class="flexbox">
            <div class="fleximg"> <img src="{{ config('constant.ASSETS_URL') }}frontend/images/CMS.png" alt="" class="img-fluid">
              <div class="plus"> <a href="#!" id="plus02"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/plus.png" alt=""></a> </div>
              <img src="{{ config('constant.ASSETS_URL') }}frontend/images/close.png" alt="" class="tutup02"> </div>
            <div class="flxtxt">
              <h4>CLAS MILD SILVER</h4>
              <p class="desc-rokok">aliquip ex ea commodo consequat.<br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br>
                cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection