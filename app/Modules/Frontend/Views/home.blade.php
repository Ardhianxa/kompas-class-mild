@extends('Frontend::global.template')     

@section('content')
<div class="satuhalaman">
  <section class="isicontent">
    <div class="section fp-auto-height" style="background-color:#4D4D4D">
      <div class="bg_splash">
        <div class="scrolldown"> <object type="image/svg+xml" data="{{ config('constant.ASSETS_URL') }}frontend/images/arrow_scroll_an.svg" width="130px"></object></div>
        <div class="row m-0 p-0 pwh"> </div>
      </div>
    </div>
  </section>
  <section>
    <div class="clip01">
      <div class="clipthumb01"> </div>
      <div class="menutx01"><a href="{{route('campaign-story')}}">CAMPAIGN<br>
        STORY </a></div>
    </div>
    <div class="clip02">
      <div class="clipthumb02"> </div>
      <div class="menutx02"><a href="{{route('class-mild-story')}}">CERITA<br>
        CLAS MILD</a></div>
    </div>
    <div class="clip03">
      <div class="clipthumb03"> </div>
      <div class="menutx03"><a href="{{route('product')}}">TENTANG<br>
        PRODUK</a></div>
    </div>
    <div class="clip04">
      <div class="clipthumb04"> </div>
      <div class="menutx04"><a href="{{route('clasmild-consumer-letter')}}">SUARA<br>
        KONSUMEN</a></div>
    </div>
    <div class="pwh-02">
      <div class="copyright"> copyright &copy; 2020 Clas Mild. All Right Reserved <br>
        <a href="#!">Privacy Policy</a> | <a href="#!">Term of Use</a> | <a href="#!">Legal</a> </div>
      <div class="row peringatan" >
        <div class="col-sm"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/neck_11.jpg" alt="" width="150px"></div>
        <div class="col-sm text-center">PERINGANTAN : <br>
          KARENA MEROKOK SAYA TERKENA KANKER TENGGOROKAN. LAYANAN BERHENTI MEROKOK (0800-0177-65661)</div>
        <div class="col-sm text-right"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/18.png" alt=""></div>
      </div>
    </div>
  </section>
</div>
@endsection