<!DOCTYPE html>
<html lang="en">
<head>
@include('Frontend::global.metadata')
@include('Frontend::global.css')
<!--JQUERY-->
<script src="{{ config('constant.ASSETS_URL') }}frontend/js/jQuery/jquery-2.2.3.min.js"></script>
</head>
<body>
    @include('Frontend::global.header') 
    @if($page == 'Frontend::home')
        @include('Frontend::global.sidemenu') 
    @endif
    @yield('content')
    @if($page != 'Frontend::home')
        @include('Frontend::global.footer')
    @endif
    @include('Frontend::global.js')
</body>
</html>
