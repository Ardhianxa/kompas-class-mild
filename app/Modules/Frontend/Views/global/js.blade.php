
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="{{ config('constant.ASSETS_URL') }}frontend/js/jquery-3.3.1.min.js"></script> 
@yield('js_script')
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="{{ config('constant.ASSETS_URL') }}frontend/js/popper.min.js"></script> 
<script src="{{ config('constant.ASSETS_URL') }}frontend/js/bootstrap-4.3.1.js"></script>

@if(request()->segment(1)=="campaign-story")
	<script src="{{ config('constant.ASSETS_URL') }}frontend/js/jquery-modal-video.min.js"></script> 
	<script src="{{ config('constant.ASSETS_URL') }}frontend/js/owl.carousel.min.js"></script> 
	<script>
		$(".js-video-button").modalVideo({
			youtube:{
				controls:1,
				nocookie: true
			}
		});
	</script> 
	<script type="text/javascript">
	jQuery(document).ready(function() {
		$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		URLhashListener: true,
		responsive:{
			0:{
				items:1,
				nav: false,
				loop:true
			},
			600:{
				items:3
			},
			1000:{
				items:3,
				dots: false,
				nav: false,
				touchDrag: false
			}
		}
	})
			
				});
		$(document).on('click', '#thn', function() {
			$(this).addClass('active').siblings().removeClass('active')
		})
		
		$(document).ready(function(){
			$('.thn01').click(function() {
				$('.bgslide-01').css({
					'background': "url('{{ config('constant.ASSETS_URL') }}frontend/images/bg-slider01.jpg') top center no-repeat",
					'background-size': "cover",
				});
			});	
			$('.thn02').click(function() {
				$('.bgslide-01').css({
					'background': "url('{{ config('constant.ASSETS_URL') }}frontend/images/bg-slider02.jpg') top center no-repeat",
					'background-size': "cover",
				});
			});	
			$('.thn03').click(function() {
				$('.bgslide-01').css({
					'background': "url('{{ config('constant.ASSETS_URL') }}frontend/images/bg-slider03.jpg') top center no-repeat",
					'background-size': "cover",
				});
			});	
		});
	</script>
@endif

<!-- One Page Sliders -->
<script type="text/javascript" src="{{ config('constant.ASSETS_URL') }}frontend/js/jquery.onepage-scroll.js"></script> 

@if(request()->segment(1)=="")
	<script type="text/javascript">
		$(document).ready(function(){
		$(".satuhalaman").onepage_scroll({
			sectionContainer: "section",
			responsiveFallback: 600,
			loop: true
		});
			});
			$(document).ready(function(){
	$("#buka-menu").click(function(){
		$(".slidemenu").addClass("openslidemenu");
		$("#tutup-menu").css({"display": "inline-block"});
		$("#buka-menu").css({"display": "none"});
	});
	$("#tutup-menu").click(function(){
		$(".slidemenu").removeClass("openslidemenu");
		$("#buka-menu").css({"display": "inline-block"});
		$("#tutup-menu").css({"display": "none"});
	});
	});
	</script>
@endif

@if(request()->segment(1)=="")
	<script type="text/javascript">
		$(document).ready(function(){
		$(".satuhalaman").onepage_scroll({
			sectionContainer: "section",
			responsiveFallback: 600,
			loop: true
		});
			});
			$(document).ready(function(){
	$("#buka-menu").click(function(){
		$(".slidemenu").addClass("openslidemenu");
		$("#tutup-menu").css({"display": "inline-block"});
		$("#buka-menu").css({"display": "none"});
	});
	$("#tutup-menu").click(function(){
		$(".slidemenu").removeClass("openslidemenu");
		$("#buka-menu").css({"display": "inline-block"});
		$("#tutup-menu").css({"display": "none"});
	});
	});
	</script>
@endif

<script>
var assets_url = "{{ config('constant.ASSETS_URL') }}";
</script>
<script src="{{ config('constant.ASSETS_URL') }}backend/js/helpers-custom.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}frontend/js/helpers.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend/js/helpers.js"></script>

<!--SWEET ALERT-->
<script src="{{ config('constant.ASSETS_URL') }}backend/plugin/sweetalert/sweetalert-dev.js"></script>

@if($page=='Frontend::consumer_letter')
  	<link href="{{ config('constant.ASSETS_URL') }}frontend/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  	<link href="{{ config('constant.ASSETS_URL') }}frontend/css/datetimepicker.css" rel="stylesheet">
  	<link href="{{ config('constant.ASSETS_URL') }}frontend/css/tempusdominus-bootstrap-4.css" rel="stylesheet">

  	<script src="{{ config('constant.ASSETS_URL') }}frontend/js/moment-with-locales.min.js"></script>
  	<script src="{{ config('constant.ASSETS_URL') }}frontend/js/tempusdominus-bootstrap-4.js"></script>

	<script type="text/javascript">
		$(function () {
		$('#datetimepicker1').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent: true
		});
		});
	</script>
  	<script src='https://www.google.com/recaptcha/api.js'></script>
  	<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDo1P6Gp5Z7jl_ICiC1klaNj_jGLfHKn_Q&callback=initAutocomplete&libraries=places&v=weekly" defer></script>
  	<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDo1P6Gp5Z7jl_ICiC1klaNj_jGLfHKn_Q&callback=initialize"></script> -->
@endif

<!-- <script type="text/javascript">
	var myFullpage = new fullpage('#fullpage', {
		responsiveWidth: 415,
		responsiveSlides: true,
		normalScrollElements: '#scrollbox',
        afterResponsive: function(isResponsive){

        }
	});
</script> -->

@if($page == 'Frontend::product')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#plus01").click(function() {
				$("#cm12box").addClass("upscale"),
				$(".desc-rokok").addClass("showup"),
				$(".tutup01").addClass("ada"),
				$("#plus01").addClass("hilang"),
				$("#cmsbox").addClass("playfadeout"),
				$("#cm16box").addClass("playfadeout"),
				$(".flexbox").addClass("flexrow"),
				$(".txhidden").addClass("hilang");
			});
		});
		
		$(document).ready(function() {
			$(".tutup01").click(function() {
				$("#cm12box").removeClass("upscale"),
				$(".desc-rokok").removeClass("showup"),
				$(".tutup01").removeClass("ada"),
				$("#plus01").removeClass("hilang"),
				$("#cmsbox").removeClass("playfadeout"),
				$("#cm16box").removeClass("playfadeout"),
				$(".flexbox").removeClass("flexrow"),
				$(".txhidden").removeClass("hilang");
			});
		});
	
		$(document).ready(function() {
			$("#plus02").click(function() {
				$("#cmsbox").addClass("upscale");
				$(".desc-rokok").addClass("showup");
				$(".flexbox").addClass("flexrow");
				$("#plus02").addClass("hilang");
				$(".tutup02").addClass("ada");
				$("#cm12box").addClass("playfadeout");
				$("#cm16box").addClass("playfadeout");
				$(".txhidden").addClass("hilang");
				
			});
		});
		
		$(document).ready(function() {
			$(".tutup02").click(function() {
				$("#cmsbox").removeClass("upscale"),
				$(".desc-rokok").removeClass("showup"),
				$(".tutup02").removeClass("ada"),
				$("#plus02").removeClass("hilang"),
				$("#cm12box").removeClass("playfadeout"),
				$("#cm16box").removeClass("playfadeout"),
				$(".flexbox").removeClass("flexrow"),
				$(".txhidden").removeClass("hilang");
			});
		});
		
		$(document).ready(function() {
			$("#plus03").click(function() {
				$("#cm16box").addClass("upscale");
				$(".desc-rokok").addClass("showup");
				$(".flexbox").addClass("flexrow");
				$("#plus03").addClass("hilang");
				$(".tutup03").addClass("ada");
				$("#cm12box").addClass("playfadeout");
				$("#cmsbox").addClass("playfadeout");
				$(".txhidden").addClass("hilang");
				
			});
		});
		
		$(document).ready(function() {
			$(".tutup03").click(function() {
				$("#cm16box").removeClass("upscale"),
				$(".desc-rokok").removeClass("showup"),
				$(".tutup03").removeClass("ada"),
				$("#plus03").removeClass("hilang"),
				$("#cm12box").removeClass("playfadeout"),
				$("#cmsbox").removeClass("playfadeout"),
				$(".flexbox").removeClass("flexrow"),
				$(".txhidden").removeClass("hilang");
			});
		});
	</script>
@endif

<script>
$(document).ready(function() {
    $('[class^=date]').keydown(validateDateField);
});
</script>


<div class="showcase sweet" style="display:none;"><a id="failed">.</a></div>
@if(Session::has('flash_success'))
  <script>
  $(document).ready(function(e){
      $('#failed').click();
  });

  document.querySelector('.showcase.sweet a').onclick = function(){
  swal("Congratulations...", "{{ Session::get('flash_success') }}", "success");
  };
  </script>
@endif

@if(Session::has('flash_failed'))
  <script>
  $(document).ready(function(e){
      $('#failed').click();
  });

  document.querySelector('.showcase.sweet a').onclick = function(){
  swal("Oops...", "{{ Session::get('flash_failed') }}", "error");
  };
  </script>
@endif