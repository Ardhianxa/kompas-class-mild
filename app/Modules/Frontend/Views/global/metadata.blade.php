<title>Clas Mild</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="title" content="{{$meta->title}}">
<meta name="keywords" content="{{$meta->keywords}}"/>
<meta name="description" content="{{$meta->description}}"/> 
<link rel="canonical" href="{{$meta->url}}"/>
<meta property="fb:app_id" content="963875223784135"/>
<meta property="fb:admins" content="Clasmild-481170931936011"/>
<meta property="fb:pages" content="Clasmild-481170931936011"/>
<meta property="og:title" content="{{$meta->title}}"/>
<meta property="og:description" content="{{$meta->description}}"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="{{$meta->url}}"/>
<meta property="og:image" content="{{$meta->image}}"> 
<meta property="og:site_name" content="clasmild">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@CLASMILD_"/>
<meta name="twitter:creator" content="@CLASMILD_">
<meta name="twitter:title" content="{{$meta->title}}">
<meta name="twitter:description" content="{{$meta->description}}">
<meta name="twitter:image" content="{{$meta->image}}">
<meta name="twitter:url" content="{{$meta->url}}" >
<meta name="twitter:domain" content="{{$meta->url}}">
<meta name="csrf-token" content={{ csrf_token() }} />