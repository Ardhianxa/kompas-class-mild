@if($page != 'Frontend::screening' && $page != 'Frontend::forbidden')
    @if($page == 'Frontend::home')
    <div class="headerhome">
    @endif
        <div class="container-fluid">
            <div class="headers">
                <div class="div_logo p-3">
                    <a href="{{route('home')}}"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/Logo-CM01.png" alt=""></a>
                </div>
                <div class="mainmenu d-flex justify-content-center"> 
                    <a class="nav-link" href="{{route('home')}}"><img src="{{ config('constant.ASSETS_URL') }}frontend/images/home_ic.png" style="margin-right: 5px;margin-top: -4px;">&nbsp;HOME</a>
                    <a href="{{route('campaign-story')}}" class="story">CAMPAIGN STORY</a> 
                    <a href="{{route('class-mild-story')}}" class="cerita">CERITA CLAS MILD</a> 
                    <a href="{{route('product')}}" class="produk">PRODUK</a> 
                    <a href="{{route('clasmild-consumer-letter')}}" class="suara">SUARA KONSUMEN</a> 
                </div>
            </div>
            <div class="navmobile">
                <nav class="navbar navbar-expand-lg navbar-light" style="background-color:#DCDCE0;"> 
                <!-- Brand --> 
                <a class="navbar-brand" href="#"></a> 
                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"> <span class="navbar-toggler-icon"></span> </button>
                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                    <li class="nav-item"> <a class="nav-link" href="{{route('home')}}">HOME</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('campaign-story')}}">CAMPAIGN STORY</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('class-mild-story')}}">CERITA CLAS MILD</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('product')}}">PRODUK</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('clasmild-consumer-letter')}}">SUARA KONSUMEN</a> </li>
                    </ul>
                </div>
                </nav>
            </div>
        </div>
    @if($page == 'Frontend::home')
    </div>
    @endif
@endif