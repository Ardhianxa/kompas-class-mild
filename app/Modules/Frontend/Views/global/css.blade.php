<link href="{{ config('constant.ASSETS_URL') }}frontend/css/bootstrap-4.3.1.css" rel="stylesheet">
<link href="{{ config('constant.ASSETS_URL') }}frontend/css/style.css" rel="stylesheet">

@if(request()->segment(1)=="campaign-story")
    <link href="{{ config('constant.ASSETS_URL') }}frontend/css/modal-video.min.css" rel="stylesheet">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}frontend/css/owl.theme.default.min.css">
    <style type="text/css">
    .story {
        color: #fff !important;
    }
    .tahunxx.active {
        color: #ce2d27 !important;
        text-decoration: underline !important;
    }
    .bgslide-01 {
        background: url("{{ config('constant.ASSETS_URL') }}frontend/images/bg-slider01.jpg") top center no-repeat;
        background-size: cover;
    }
    </style>
@elseif(request()->segment(1)=="class-mild-story")
    <style type="text/css">
    .cerita {
        color: #fff !important;
    }
    </style>
@elseif(request()->segment(1)=="product")
    <style type="text/css">
		.produk {
			color:#fff !important;
		}
    </style>
@elseif(request()->segment(1)=="clasmild-consumer-letter") 
    <style type="text/css">
		.suara {
			color:#fff !important;
		}
    </style>
@endif

@if($page != 'Frontend::product')
    <link href="{{ config('constant.ASSETS_URL') }}frontend/css/clipstyle.css" rel="stylesheet">
@endif

@if($page == 'Frontend::home')
    <link href="{{ config('constant.ASSETS_URL') }}frontend/css/onepage-scroll.css" rel="stylesheet" type="text/css">
    <link href="{{ config('constant.ASSETS_URL') }}frontend/css/clipstyle.css" rel="stylesheet">
@endif
<!--SWEET ALERT-->
<link href="{{ config('constant.ASSETS_URL') }}backend/plugin/sweetalert/sweetalert.css" rel="stylesheet" >