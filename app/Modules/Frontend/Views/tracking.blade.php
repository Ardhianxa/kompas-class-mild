@extends('Frontend::global.template')     

@section('content')
<script>
function tracking_consumer_letter(id)
{
    if(id != '')
    {
        window.location = "{{config('api.APP_URL')}}/consumer-letter-tracking/"+id;
    }
}
</script>
<section class="suarabox">
  <div class="container text-center">
    <div class="row">
      <div class="col">
        <h2>TRACKING ID</h2>
      </div>
      <div class="row mt-5">
        <div class="col">
          <p>
          <h4>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="container mt-5">
    <div class="row">
      <div class="col">
        <h3 class="text-center">Masukkan ID Pesan Anda :</h3>
        <p>
        <div class="form-group">
          <label for="tx01">INPUT ID</label>
          <input type="text" class="form-control" id="find-id-tracking" value="{{$id}}" aria-describedby="emailHelp" placeholder="">
          <small id="emailHelp" class="form-text text-muted">*Pesan ID Dikirim VIA EMail Anda Yang Sudah Terdaftar</small> </div>
        </p>
        <!-- <p>
        <h4>GOOGLE RECAPTHA HERE</h4>
        </p> -->
        <p>
          <button type="button" onclick="tracking_consumer_letter(document.getElementById('find-id-tracking').value)" class="btn btn-primary btn-lg">CARI</button>
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col bg-light mb-5">
        <table class="table table-striped table-responsive-sm">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">NAMA</th>
              <th scope="col">JENIS PERTANYAAN</th>
              <th scope="col">JUDUL PESAN</th>
				<th scope="col">EMAIL</th>
				<th scope="col">STATUS</th>
            </tr>
          </thead>
          <tbody>
            @if($id != null && count($data) > 0)
                @foreach($data as $value)
                <tr>
                    <th scope="row">id-{{consumer_tracking_id_show($value->tracking_id)}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{consumerLetterTitle($value->tujuan_pernyataan)}}</td>
                    <td>{{consumer_letter_purpose($value->judul)}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{consumerLetterStatus($value->status)}}</td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6"><div align="center">-No Data-</div></td>
                </tr>
		    @endif
          </tbody>
        </table>
        <div class="card-footer text-right">
            <nav class="d-inline-block">
                {{ $data->links() }}
            </nav>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection