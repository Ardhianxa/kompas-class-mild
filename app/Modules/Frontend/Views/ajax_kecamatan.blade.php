<label for="tx04">Kecamatan</label>
<select class="form-control" id="tx04" required="required" name="kecamatan" onchange="get_kodepos('{{$provinsi}}','{{$kabupaten}}',this.value,'kodepos','{{ route('get-kodepos') }}')">
    <option value="">-Pilih kecamatan-</option>
    @foreach($kecamatan as $kecamatan_data)
        <option value="{{$kecamatan_data->kecamatan}}">{{$kecamatan_data->kecamatan}}</option>
    @endforeach
</select>