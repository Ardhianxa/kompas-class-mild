@extends('Frontend::global.template')     

@section('content')
    @if(isset($data->text))
        <section class="bg_detailcerita">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <div class="isi-cerita">
                                {{$data->text}}
                            </div>
                            <style>
                                .isi-video {
                                    position: relative;
                                    padding-bottom: 56.25%; /* 16:9 */
                                    height: 0;
                                }
                                .isi-video iframe {
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                    width: 100%;
                                    height: 100%;
                                }
                            </style>
                            <div class="isi-video">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$data->video}}?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection