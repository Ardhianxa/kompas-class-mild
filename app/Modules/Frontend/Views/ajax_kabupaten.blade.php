<label for="tx04">Kabupaten</label>
<select class="form-control" id="tx04" name="kabupaten" required="required" onchange="get_kecamatan('{{$provinsi}}',this.value,'kecamatan','{{ route('get-kecamatan') }}')">
    <option value="">-Pilih Kabupaten-</option>
    @foreach($kabupaten as $kabupaten_data)
        <option value="{{$kabupaten_data->kabupaten}}">{{$kabupaten_data->kabupaten}}</option>
    @endforeach
</select>