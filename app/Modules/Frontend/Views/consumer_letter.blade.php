@extends('Frontend::global.template')     

@section('content')
<section class="suarabox">
    <div class="container text-center">
        <div class="row">
            <div class="col">
                <h2>SUARA KONSUMEN</h2>
            </div>
            <div class="row mt-5">
                <div class="col">
                    <p>
                        <h4>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</h4>
                        <p class="mt-5">
                            <a href="{{route('tracking')}}" class="btn btn-outline-info">CHECK TRACKING ANDA DISINI</a>
                        </p>
                    </p>
                    <p class="mt-5">
                        <h3 class="text-center">Masukkan Data Anda*</h3>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <form method="POST" enctype="multipart/form-data" action="{{route('clasmild-consumer-letter-post')}}">
                    @csrf
                    <div class="form-group">
                        <label for="tx01">Nama</label>
                        <input type="text" class="form-control" id="tx01" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="tx02">email</label>
                        <input type="text" class="form-control" id="tx02" name="email">
                    </div>
                    <div class="form-group">
                        <label for="tx03">provinsi</label>
                        <select class="form-control" id="tx04" name="provinsi" id="provinsi" required="required" onchange="get_kabupaten(this.value,'kabupaten','{{ route('get-kabupaten') }}')">
                            <option value="">-Pilih Provinsi-</option>
                            @foreach($provinsi as $provinsi_data)
                                <option value="{{$provinsi_data->provinsi}}">{{$provinsi_data->provinsi}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group kabupaten">
                        <label for="tx04">Kabupaten</label>
                        <select class="form-control" id="tx04" name="kabupaten" required="required">
                            <option value="">-Pilih Kabupaten-</option>
                        </select>
                    </div>
                    <div class="form-group kecamatan">
                        <label for="tx04">Kecamatan</label>
                        <select class="form-control" id="tx04" name="kecamatan" required="required">
                            <option value="">-Pilih Kecamatan-</option>
                        </select>
                    </div>
                    <div class="form-group kodepos">
                        <label for="tx04">Kode pos</label>
                        <select class="form-control" id="tx04" name="kode_pos" required="required">
                            <option value="">-Pilih Kodepos-</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tx06">Alamat Lengkap</label>
                        <textarea class="form-control" id="tx06" rows="3" name="alamat"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="tx07">No. smartphone (Whatsapp aktif)</label>
                        <input type="text" class="form-control" id="tx07"name="telepon" >
                    </div>
                    <div class="separates-border"> </div>
                    <div class="form-group">
                        <label for="tx08">tujuan pernyataan*</label>
                        <select class="form-control" onchange="tujuan(this.value)" id="tx08" name="tujuan_pernyataan">
                            <option value=0>-Pilih-</option>
                            <option value=1>Apresiasi</option>
                            <option value=2>Komplain</option>
                            <option value=3>Pertanyaan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tx08">silahkan pilih pesan*</label>
                        <select class="form-control judul" id="tx08" onchange="consumer_letter_type(this.value)" name="judul">
                            <option>-Pilih-</option>
                            <option value=1>Periklanan</option>
                            <option value=2>Ketersediaan Produk</option>
                            <option value=3>Kualitas Produk</option>
                        </select>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="additional3">
                                <div class="form-group">
                                    <label for="tx09">nama produk</label>
                                    <select class="form-control form-control-lg" id="sel1" name="nama_produk">
                                        <option value="">-Pilih-</option>
                                        <option>Clas Mild 16</option>
                                        <option>Clas Mild 12</option>
                                        <option>Clas Mild Silver</option>
                                        <!-- <option>A Mild</option>
                                        <option>Dunhill Mild</option>
                                        <option>Dunhill Filter</option>
                                        <option>Magnum Mild</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="additional">
                                <div class="form-group">
                                    <label for="tx10">tanggal produksi</label>
                                    <input name="tanggal_produksi" type="text" class="date form-control datetimepicker-input input-md" id="datetimepicker1" data-toggle="datetimepicker" data-target="#datetimepicker1"/>
                                </div>
                                <div class="form-group">
                                    <label for="tx11">Kode produksi</label>
                                    <input type="text" class="form-control" id="tx11" name="kode_produksi">
                                </div>
                            </div>
                            <div class="additional2">
                                <div class="form-group">
                                    <label for="tx11">Lokasi</label>
                                    <label for="merk">Longitude & Latitude (KLIK DI PETA)</label>
                                    <input type="text" class="form-control" id="latlong" name="latlong" value="" readonly="readonly">
                                    <input type="hidden" id="longitude" name="longitude" value="">
                                    <input type="hidden" id="latitude" name="latitude" value="">
                                    <input type="hidden" id="submit" class="btn btn-primary btn-flat">
                    
                                    <div id="map_canvas" style="width: 100%; height: 600px"></div>
                                    <p></p>
                                    <div class="form-group">
                                        <textarea class="form-control" id="pac-input" rows="3" placeholder="Alamat Lengkap Lokasi Pembelian" name="lokasi_pembelian"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p></p>
                    <div class="form-group">
                    <label for="tx14">Pesan Anda</label>
                    <textarea class="form-control" id="tx14" rows="4" name="pesan"></textarea>
                    </div>
                    <div class="input-group mb-3">
                    <div class="input-group-prepend"> <span class="input-group-text">Upload</span> </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="tx15" name="lampiran" accept=".png, .jpg, .jpeg, .pdf, .zip, .word, .ppt">
                        <label class="custom-file-label" for="tx15">Pilih file upload. (*ukuran max. {{config('constant.MAX_SIZE_IMAGE_UPLOAD')}}MB Format : .jpg/.png/.pdf/.zip/.word/.ppt)</label>
                    </div>
                    </div>
                    <div class="form-group">
                        {!! app('captcha')->display() !!}
                    </div>
                    <p><input type="submit" class="btn btn-primary btn-lg"></input></p>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js_script')
<script>

</script>
@endsection