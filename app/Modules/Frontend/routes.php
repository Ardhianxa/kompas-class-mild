<?php
Route::group(  
[
	'namespace' => 'App\Modules\Frontend\Controllers', 
	'prefix' => '', 
	'middleware' => [
						'web',
						'before_global',
						'after_global',
						'App\Modules\Frontend\Middleware\BeforeMiddleware',
						'App\Modules\Frontend\Middleware\AfterMiddleware'
					]
], 
function ()
{
	Route::get('/', [
		'as' => 'home', 'uses' => 'HomeController@index'
	]);

	Route::get('screening', [
		'as' => 'screening', 'uses' => 'HomeController@screening'
	]);

	Route::get('forbidden', [
		'as' => 'forbidden', 'uses' => 'HomeController@forbidden'
	]);

	Route::post('screening', [
		'as' => 'screening-post', 'uses' => 'HomeController@screening_post'
	]);

	Route::get('campaign-story', [
		'as' => 'campaign-story', 'uses' => 'HomeController@campaign'
	]);

	Route::get('class-mild-story', [
		'as' => 'class-mild-story', 'uses' => 'HomeController@clas_mild_story'
	]);

	Route::get('kodepos', [
		'as' => 'kodepos', 'uses' => 'HomeController@kodepos'
	]);

	Route::get('product', [
		'as' => 'product', 'uses' => 'HomeController@product'
	]);

	Route::get('consumer-letter-tracking/{id?}', [
		'as' => 'tracking', 'uses' => 'HomeController@tracking'
	]);

	Route::get('clasmild-consumer-letter', [
		'as' => 'clasmild-consumer-letter', 'uses' => 'HomeController@consumer_letter'
	]);

	Route::post('clasmild-consumer-letter-post', [
		'as' => 'clasmild-consumer-letter-post', 'uses' => 'HomeController@consumer_letter_post'
	]);

	Route::get('get-kabupaten', [
		'as' => 'get-kabupaten', 'uses' => 'HomeController@ajax_kabupaten'
	]);

	Route::get('get-kecamatan', [
		'as' => 'get-kecamatan', 'uses' => 'HomeController@ajax_kecamatan'
	]);

	Route::get('get-kodepos', [
		'as' => 'get-kodepos', 'uses' => 'HomeController@ajax_kodepos'
	]);
});
?>