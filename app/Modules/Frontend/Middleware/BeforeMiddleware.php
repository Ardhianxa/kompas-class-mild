<?php

namespace App\Modules\Frontend\Middleware; 

use Closure;
use App\Modules\Frontend\Models\Frontend;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$request->session()->forget('cekUsia');
        //screening usia
        $cekUsia = Frontend::cekUsia();
        if($request->route()->getName() == 'screening' || $request->route()->getName() == 'screening-post'){
            if($cekUsia != ''){
                if($cekUsia < 18){
                    return redirect()->route('forbidden'); 
                }
                else{
                    return redirect()->route('home'); 
                }   
            }
        }
        elseif($request->route()->getName() == 'forbidden'){
            if($cekUsia == ''){
                return redirect()->route('screening'); 
            }
            else{
                if($cekUsia >= 18){
                    return redirect()->route('home'); 
                }
            }
        }
        else{
            if($cekUsia == ''){
                return redirect()->route('screening'); 
            }
            elseif($cekUsia < 18){
                return redirect()->route('forbidden'); 
            }
        }

        return $next($request);
    }
}
