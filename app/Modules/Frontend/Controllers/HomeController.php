<?php
 
namespace App\Modules\Frontend\Controllers;

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Frontend\Models\Frontend;
use App\Modules\Frontend\Models\Screening;
use App\Modules\Frontend\Models\Consumer_Letter;
use Validator;
use Mail;
use Session;
use DB;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $meta = $this->meta('home');
        $page = 'Frontend::home';
        return view($page,compact('page','meta'));
    }

    public function forbidden(Request $request)
    {
        $page = 'Frontend::forbidden';
        $meta = $this->meta('home');
        return view($page,compact('page','meta'));
    }

    public function screening(Request $request)
    {
        $page = 'Frontend::screening';
        $meta = $this->meta('home');
        return view($page,compact('page','meta'));
    }

    public function screening_post(Request $request)
    {
        if(strlen($request->tanggal)==1){$request->tanggal = '0'.$request->tanggal;}
        $insert = new Screening();
        $insert->tanggal_lahir = $request->tahun.'-'.$request->bulan.'-'.$request->tanggal;
        $insert->perokok = $request->anda_merokok;
        $insert->merek_rokok = $request->merek_rokok;
        $insert->status = 1;

        if($insert->save()){
            $age = age($insert->tanggal_lahir);
            $request->session()->put('cekUsia', $age);
            if($age < 18){
                return redirect()->route('forbidden');
            }
            else{
                return redirect()->route('home');
            }
        }
    }

    public function product(Request $request)
    {
        $page = 'Frontend::product';
        $meta = $this->meta('product');
        return view($page,compact('page','meta'));
    }

    public function clas_mild_story(Request $request)
    {
        $page = 'Frontend::story';
        $data = Frontend::story();
        $meta = $this->meta('story',$data);
        return view($page,compact('page','meta','data'));
    }
    
    public function tracking(Request $request, $id = null)
    {
        $meta = $this->meta('home');
        $page = 'Frontend::tracking';
        $data = Frontend::consumer_letter_data($id);
        return view($page,compact('page','meta','data','id'));
    }

    public function campaign()
    {
        $meta = $this->meta('campaign');
        $page = 'Frontend::campaign';
        
        return view($page,compact('page','meta'));
    }

    public function ajax_kabupaten(Request $request)
    {
        $provinsi = $request->provinsi_data;
        $kabupaten = Frontend::kabupaten($provinsi);

        return view('Frontend::ajax_kabupaten',compact('kabupaten','provinsi'));
    }

    public function ajax_kecamatan(Request $request)
    {
        $kabupaten = $request->kabupaten_data;
        $provinsi = $request->provinsi_data;
        $kecamatan = Frontend::kecamatan($provinsi,$kabupaten);

        return view('Frontend::ajax_kecamatan',compact('kecamatan','provinsi','kabupaten'));
    }

    public function ajax_kodepos(Request $request)
    {
        $kabupaten = $request->kabupaten_data;
        $provinsi = $request->provinsi_data;
        $kecamatan = $request->kecamatan_data;
        $kodepos = Frontend::kodepos($provinsi,$kabupaten,$kecamatan);

        return view('Frontend::ajax_kodepos',compact('kodepos'));
    }

    public function consumer_letter()
    {
        $provinsi = Frontend::provinsi();
        $meta = $this->meta('home');
        $page = 'Frontend::consumer_letter';
        return view($page,compact('page','meta','provinsi'));
    }

    public function consumer_letter_post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required|captcha',
            'lampiran' => 'file|mimes:jpg,png,pdf,zip,word,ppt|max:'.(config('constant.MAX_SIZE_IMAGE_UPLOAD')*1000)
        ]);

        // if ($validator->fails()) {
        //     return redirect()->route('clasmild-consumer-letter')->with('flash_failed', 'Captcha Not Valid');
        // } 

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->route('clasmild-consumer-letter')->with('flash_failed', 'Email Not Valid');
        }
        else{
            $save = new Consumer_Letter();
            $destinationPath = config('constant.ACTUAL_PATH').'public/assets/uploads/img/consumer_letter_attachment/';
            if($request->file('lampiran') != "")
            {
                $filename = md5(time()).'.'.$request->file('lampiran')->getClientOriginalExtension();
                if($request->file('lampiran')->move($destinationPath, $filename))
                {
                    $save->user_attachment = $filename;
                }
                else{
                    return redirect()->route('clasmild-consumer-letter')->with('flash_failed', 'Upload File Failed');
                }
            }

            $save->nama = $request->nama;
            $save->tracking_id = md5(date('Y-m-d H:i:s'));
            $save->email = $request->email;
            $save->alamat = $request->alamat;
            $save->kota = $request->kabupaten;
            $save->telepon = $request->telepon;
            $save->judul = $request->judul;
            $save->pesan = $request->pesan;
            $save->balasan = '';
            $save->attachment = '';
            $save->kecamatan = $request->kecamatan;
            $save->kelurahan = "";
            $save->provinsi = $request->provinsi;
            $save->kode_pos = $request->kodepos;
            $save->tujuan_pernyataan = $request->tujuan_pernyataan;
            $save->nama_produk = $request->nama_produk;
            $save->lokasi_pembelian = $request->lokasi_pembelian;
            $save->lat = 0;
            $save->lon = 0;
            
			if($request->latlong != ''){
				$latlong = str_replace("(","", $request->latlong);
	            $latlong = str_replace(")","", $latlong);
	            $latlongPecah = explode(",",$latlong);
	            if(count($latlongPecah) > 0){
	            	$save->lat =  trim($latlongPecah[0]);
	            	$save->lon =  trim($latlongPecah[1]);
	            }
            }
            $save->tanggal_produksi = date('Y-m-d', strtotime($request->tanggal_produksi));
            $save->kode_produksi = $request->kode_produksi;
            $save->status = 1;
            if($save->save()){
                $value = array(
                    'tracking_id' => strtoupper(consumer_tracking_id_show($save->tracking_id))
                );
                $send = Mail::send('Frontend::consumer_letter_thanks', $value, function ($message) use ($request,$destinationPath)
                {
                    $message->from(config('constant.MAIL_FROM'), config('constant.APP_NAME'));
                    $message->to($request->email)->subject('CLAS MILD CS');
                });
            }

            return redirect()->route('clasmild-consumer-letter')->with('flash_success', 'Successfully');
        }
    }
}
