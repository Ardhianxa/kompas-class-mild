<?php

namespace App\Modules\Inbox\Models;

use Illuminate\Database\Eloquent\Model;

class Privileges extends Model
{
    protected $table = 'privileges';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
		'data',
        'status'
    ];

    protected $hidden = [
    ];

    public static function Data($type,$type2,$status,$key)
    {
    	$data = Self::select('id','name','data','created_at')->where('status',$status);

        if(strtolower($type)==$type2 && $key != null){
            $data->whereRaw("
            (
                name LIKE '%".$key."%'
            )
            ");
        }

        return $data->paginate(config('constant.LIMIT_DATA_TABLE'));
    }
}
