<?php

namespace App\Http\Middleware;  

use Closure;
use Session; 
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Guard;
use Helpers;

class PrivilegesMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $data) 
    {
        $data = explode(":",$data);
        if(Helpers::privileges($data[0],$data[1])){
            return $next($request);
        }
        return redirect()->route('logout');
    }
}
