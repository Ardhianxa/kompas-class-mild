<?php

namespace App\Http\Middleware; 

use Closure;
use Auth;
use Helpers;

class AfterGlobalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        Helpers::activity($request);
        Helpers::cacheRemove($request);
        return $response;
    }
}
