<?php

namespace App\Http\Middleware; 

use Closure;
use Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Guard;

class SessionlMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //code here
        $session = array(
            'email'
        );

        $error = false;
        foreach($session as $session_value){
            if(Session::get($session_value) == null || Session::get($session_value) == ''){
                $error = true;
            }
        }
        
        if(!$error){
            return $next($request);
        }
        
        return redirect()->route('logout');
    }
}
