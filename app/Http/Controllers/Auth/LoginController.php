<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Models\UsersInfo;
use Illuminate\Validation\ValidationException;
use Helpers;
use Auth;
use Log;
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('after_global')->except('showLoginForm','sendFailedLoginResponse');
    }

    public function showLoginForm()
    {
        $title = "LOGIN";
        $page = 'login';
        return view('auth.login',compact('title','page'));
    }

    public function logout(Request $request)
    {

        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/login');
    }

    public function authenticated(Request $request, $user)
    {
        $request->session()->regenerate(); 
        
        //custom login session
        $userInfo = UsersInfo::
        select('email','first_name','last_name','handphone','image','users.created_at','status')
        ->join('users','users.id','=','users_info.user_id')
        ->where('users.id',Auth::id())
        ->first();

        if($userInfo->status != 1){
            return redirect()->route('logout');
        }

        if($request->remember=='on'){
            Cookie::queue(Cookie::make(config('constant.APP_NAME').'-username', $request->email, 525600));
            Cookie::queue(Cookie::make(config('constant.APP_NAME').'-password', $request->password, 525600));
        }

        if(isset($userInfo->email)){
            $request->session()->put('email', $userInfo->email);
            $request->session()->put('first_name', $userInfo->first_name);
            $request->session()->put('last_name', $userInfo->last_name);
            $request->session()->put('handphone', $userInfo->handphone);
            $request->session()->put('image', $userInfo->image);
            $request->session()->put('register_date', date('d-m-Y H:i',strtotime($userInfo->created_at)));
            $request->session()->put('last_login', date('d-m-Y H:i'));
        }

        $route = "home-admin";
        return redirect()->route($route);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->flash('flash_failed', 'Login Failed!');
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
}
