<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use stdClass;
use App\Modules\Frontend\Models\Frontend;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        error_reporting(0);
    }

    public function getFromRedis($keys, $debug = false)
    {
        if ($debug)
            dd(Redis::get($keys));

        return Redis::get($keys);
    }
    
    public function setToRedis($key, $value, $ttl = null)
    {
        if ($value) {
            if ($ttl == null) {
                $ttl = env('REDIS_TTL', 1) * 300;
            } else {
                $ttl = env('REDIS_TTL', 1) * $ttl;
            }
            try {
                app('redis')->set($key, $value);
                app('redis')->expireat($key, time() + $ttl);
            } catch (\Exception $e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public function meta($act, array $params = array())
    {
        if($act=='home'){
            $result = new stdClass();
            $result->title = 'Clas Mild - Rokok Ternama di Indonesia';
            $result->keywords = 'clasmild, rokok, indonesia, tembakau';
            $result->description = 'Clas Mild - Rokok Ternama di Indonesia';
            $result->url = config('constant.APP_URL');
            $result->image = config('constant.ASSETS_URL').'frontend/images/Logo-CM01.png';
        }
        elseif($act=='campaign'){
            $result = new stdClass();
            $result->title = 'Clas Mild - Rokok Ternama di Indonesia';
            $result->keywords = 'clasmild, rokok, indonesia, tembakau';
            $result->description = 'Clas Mild - Rokok Ternama di Indonesia';
            $result->image = config('constant.ASSETS_URL').'frontend/images/Logo-CM01.png';
            $result->url = config('constant.APP_URL').'/campaign-story';
        }
        elseif($act=='story'){
            $result = new stdClass();
            $result->title = $params['title'];
            $result->keywords = $params['keywords'];
            $result->description = $params['text'];
            $result->image = config('constant.ASSETS_URL').'frontend/images/story/'.$params['image'];
            $result->url = config('constant.APP_URL').'/class-mild-story';
        }
        elseif($act=='product'){
            $result = new stdClass();
            $result->title = 'Clas Mild - Rokok Ternama di Indonesia';
            $result->keywords = 'clasmild, rokok, indonesia, tembakau';
            $result->description = 'Clas Mild - Rokok Ternama di Indonesia';
            $result->url = config('constant.APP_URL');
            $result->image = config('constant.ASSETS_URL').'frontend/images/Logo-CM01.png';
        }
        
        return $result;
    }
}
