<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\FileManager;
use Auth;
use Helpers;
use Validator;

class FilemanagerController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function go(Request $request)
    {
        $message = "";
        $result_validation = true;
        $filename = $request->filename;
        $caption = $request->caption;
        $config = json_decode($request->config);
        $config_original = $request->config;
        $container = $config->container;
        $container_image = $config->container_image;
        $crop = $config->crop;
        $validation = $config->validation;
        $editor = $config->editor;
        $config_name = $config->config_name;
        $file_manager_id = $request->file_manager_id;
        $file_manager_id_name = $config->file_manager_id_name;

        $komponen = array();
        $width = array();
        $height = array();

        //disini lanjut kondisi append

        if(!$editor){

            //manual crop
            if(isset($crop->status) && $crop->status){
                foreach($crop->data as $crop_key => $crop_value){
                    $crop_value->segment = str_replace("/","garing",$crop_value->segment);
                    $komponen[] = $crop_value->segment;
                    $width[] = $crop_value->width;
                    $height[] = $crop_value->height;
                }
            }

            //validation
            list($validation_width, $validation_height, $type, $attr) = getimagesize(config('constant.ACTUAL_PATH').'public/assets/uploads/img/original/'.$filename);
            if(!empty($validation->status)){
                if($validation->status){
                    if($validation->data->orientation=="landscape"){
                        if($validation_height > $validation_width){
                            $result_validation = false;
                        }
                    }
                    elseif($validation->data->orientation=="portrait"){
                        if($validation_width > $validation_height){
                            $result_validation = false;
                        }
                    }
                    elseif($validation->data->orientation=="squere"){
                        if($validation_width != $validation_height){
                            $result_validation = false;
                        }
                    }

                    if(!$result_validation){
                        $message = "Pilih foto dengan orientasi ".$validation->data->orientation;
                    }

                    if(!empty($validation->data->min_resolution)){
                        if($validation_width < $validation->data->min_resolution->width || $validation_height < $validation->data->min_resolution->height){
                            $result_validation = false;
                            $message .= ", Pilih foto dengan minimal ukuran, width : ".$validation->data->min_resolution->width." dan height : ".$validation->data->min_resolution->height;
                        }
                    }
                }
            }
        }

        $result = json_encode(array(
            'container' => $container,
            'result_validation' => $result_validation,
            'editor' => $editor,
            'status' => 'ok',
            'message' => $message,
            'html' => view('segment.file-manager-go',compact('file_manager_id_name','file_manager_id','config_name','config_original','filename','crop','validation','komponen','width','height','editor','caption','container_image'))->render()
        ));

        echo $result;
    }

    public function get(Request $request)
    {
        $data = FileManager::Data($request->key,$request->type,$request->order,$request->page);
        $order = $request->order;
        $key = $request->key;
        $config = json_decode($request->config);

        if($request->type=='image'){
            $page = 'segment.file-manager-image';
        }
        elseif($request->type=='video'){
            $page = 'segment.file-manager-video';
        }
        elseif($request->type=='others'){
            $page = 'segment.file-manager-others';
        }
        return view($page ,compact('data','order','key','config'));
    }

    public function add(Request $request)
    {
        $config = json_encode(json_decode($request->config));
        return view('segment.file-manager-add-ajax',compact('config'));
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $config = json_encode(json_decode($request->config));
        $data = FileManager::edit($request->id);
        return view('segment.file-manager-edit-ajax',compact('data','id','type','config'));
    }

    public function update(Request $request)
    {
        //update data
        $data = FileManager::find($request->id);
        $type = $request->type;
        if($type=='image'){$type=1;}elseif($type=='video'){$type=2;}elseif($type=='others'){$type=3;}
        $data->type = $type;
        $data->title = $request->title;
        $data->caption = $request->caption;
        $data->note = $request->note;
        $data->share = $request->share;


        $valid = true;
        if($request->file('upfile') != "")
        {
            $validator = Validator::make($request->all(), [
                'upfile' => 'file|max:'.(config('constant.MAX_SIZE_IMAGE_UPLOAD')*1000),
            ]);
    
            if ($validator->fails()) {
                $valid = false;
            }
            else{
                $destinationPath = config('constant.ACTUAL_PATH').'public/assets/uploads/img/original/';
                $filename = md5(time()).'.'.$request->file('upfile')->getClientOriginalExtension();
                if(!$request->file('upfile')->move($destinationPath, $filename)){
                    $valid = false;
                }
                else{
                    $data->filename = $filename;
                    if($request->watermark==1)
                    {
                        Helpers::watermark($destinationPath,$filename,$request->watermark_position);
                    }
                }
            }
        }
        
        if($valid){
            $data->save();
            echo 'ok';
        }
        else{
            echo 'Internal Server Error';
        }
    }

    public function store(Request $request)
    {
        if($request->file('upfile') != "")
        {
            $validator = Validator::make($request->all(), [
                'upfile' => 'file|max:'.(config('constant.MAX_SIZE_IMAGE_UPLOAD')*1000),
            ]);
    
            if ($validator->fails()) {
                echo 'Maksimal Attachment '.config('constant.MAX_SIZE_IMAGE_UPLOAD').' MB';
            }
            else{
                $destinationPath = config('constant.ACTUAL_PATH').'public/assets/uploads/img/original/';
                $filename = md5(time()).'.'.$request->file('upfile')->getClientOriginalExtension();
                if($request->file('upfile')->move($destinationPath, $filename)){
                    $type = 'image';
                    if($type=='image'){$type=1;}elseif($type=='video'){$type=2;}elseif($type=='others'){$type=3;}
                    $save = new FileManager();
                    $save->type = $type;
                    $save->title = $request->title;
                    $save->caption = $request->caption;
                    $save->note = $request->note;
                    $save->share = $request->share;
                    $save->filename = $filename;
                    $save->created_id = Auth::id();
                    $save->status = 1;
                    $save->save();
                    
                    if($request->watermark==1)
                    {
                        Helpers::watermark($destinationPath,$filename,$request->watermark_position);
                    }

                    echo "ok";
                }
                else{
                    echo 'Failed Upload File';
                }
            }
        }
        else{
            echo 'Tentukan File Anda';
        }
    }
}
