<?php
function unslug($text){
    return str_replace('-',' ',$text);
}

function comment_time($timestamp){

    //type cast, current time, difference in timestamps
    $timestamp      = (int) $timestamp;
    $current_time   = time();
    $diff           = $current_time - $timestamp;
    
    //intervals in seconds
    $intervals      = array (
        'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
    );
    
    //now we just find the difference
    if ($diff == 0)
    {
        return 'just now';
    }    
    if ($diff < 60)
    {
        return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
    }        
    if ($diff >= 60 && $diff < $intervals['hour'])
    {
        $diff = floor($diff/$intervals['minute']);
        return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
    }        
    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
    {
        $diff = floor($diff/$intervals['hour']);
        return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
    }    
    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
    {
        $diff = floor($diff/$intervals['day']);
        return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
    }    
    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
    {
        $diff = floor($diff/$intervals['week']);
        return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
    }    
    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
    {
        $diff = floor($diff/$intervals['month']);
        return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
    }    
    if ($diff >= $intervals['year'])
    {
        $diff = floor($diff/$intervals['year']);
        return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
    }
}

function validateDate($date, $format = 'Y-m-d H:i:s') 
{
    $d = DateTime::createFromFormat($format, $date);
    if($d && $d->format($format) == $date){
        $result = true;
    }
    else{
        $result = false;
    }
    return $result;
}

function date_with_hours($date)
{
    if($date != ""){
        $tgl = substr($date,8,2);
        $bln = substr($date,5,2);
        $thn = substr($date,0,4);
        $jam_menit = substr($date,11,5);

        return $tgl.'-'.month($bln).'-'.$thn.' '.$jam_menit;
    }
}

function date_without_hours($date)
{
    $tgl = substr($date,8,2);
    $bln = substr($date,6,2);
    $thn = substr($date,0,4);

    return $tgl.'-'.month($bln).'-'.$thn;
}

function hours($date)
{
    $jam_menit = substr($date,11,5);

    return $jam_menit;
}

function month($month)
{
    if($month==1 || $month=='01'){
        $result = "Januari";
    }
    elseif($month==2 || $month=='02'){
        $result = "Februari";
    }
    elseif($month==3 || $month=='03'){
        $result = "Maret";
    }
    elseif($month==4 || $month=='04'){
        $result = "April";
    }
    elseif($month==5 || $month=='05'){
        $result = "Mei";
    }
    elseif($month==6 || $month=='06'){
        $result = "Juni";
    }
    elseif($month==7 || $month=='07'){
        $result = "Juli";
    }
    elseif($month==8 || $month=='08'){
        $result = "Agustus";
    }
    elseif($month==9 || $month=='09'){
        $result = "September";
    }
    elseif($month==10){
        $result = "Oktober";
    }
    elseif($month==11){
        $result = "November";
    }
    elseif($month==12){
        $result = "Desember";
    }

    return $result;
}

function curl($method,$url,$headers,$fields)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);  
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    $result = curl_exec($ch);           
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);

    echo $result;
}

function generateSessId(){
    $token = base64_encode(openssl_random_pseudo_bytes(20));
    return $token;
}

function generateToken(){
    $token = bin2hex(random_bytes(64));
    return $token;
}

function ssl_private_encrypt($payload,$encryptedData,$private_key)
{
    return openssl_private_encrypt(json_encode($payload), $encryptedData, $private_key);
}

function ssl_decrypt($txt,$chiper_aes,$token,$iv, $tag, $sess_id)
{
    return openssl_decrypt($txt, $chiper_aes, base64_decode($token), $options=0, $iv, $tag, $sess_id);
}

function ssl_encrypt($jwt,$token,$sess_id,$chiper_aes)
{  
    $ivlen = openssl_cipher_iv_length($chiper_aes);
    $iv = openssl_random_pseudo_bytes($ivlen);
    $iv_base64 = base64_encode($iv);
    $ciphertext = openssl_encrypt($jwt, $chiper_aes, base64_decode($token), $options=0, $iv, $tag, $sess_id);
    
    return $ciphertext.'|'.$iv_base64.'|'.base64_encode($tag).'|'.$sess_id;
}

function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        case "GET":
            curl_setopt($curl,CURLOPT_URL,$url);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    if($httpCode == 404) {
        return "404";
    }
    else
    {
        return $result;
    }
}

function validateArray($rules, $array)
{
    $errors = [];

    foreach ($rules as $key => $value) {
        if ($rules[$key] == 'required') {
            if (!isset($array[$key])) {
                array_push($errors, $key.' is required');
            }elseif($array[$key] == null){
                array_push($errors, $key.' is required');
            }elseif($array[$key] == false){
                array_push($errors, $key.' is required');
            }
        }
    }

    if (count($errors) != 0) {
        $response = (object)[
            'error' => true,
            'errors' => $errors,
        ];
    }else{
        $response = (object)[
            'error' => false,
            'errors' => $errors,
        ];
    }

    return $response;
}
