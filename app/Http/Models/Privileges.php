<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Models\Role_deleted;

class Privileges extends Model
{
    protected $table = 'privileges';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
		'data',
        'status'
    ];

    protected $hidden = [
    ];

    public static function Data($type,$type2,$status,$key)
    {
        if(config('constant.CACHE')){
            $cache_name = "privileges_data_".$type."_".$type2."_".$status."_".$key;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Data_data($type,$type2,$status,$key), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data($type,$type2,$status,$key);
        }

        return $data;
    }

    public static function Data_data($type,$type2,$status,$key)
    {
        $data = Role::where('status',$status);

        if(strtolower($type)==$type2 && $key != null){
            $data->whereRaw("
            (
                name LIKE '%".$key."%'
            )
            ");
        }

        return $data->paginate(config('constant.LIMIT_DATA_TABLE'));
    }

    public static function List()
    {
        if(config('constant.CACHE')){
            $cache_name = "privileges_list";
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::List_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::List_data();
        }

        return $data;
    }

    public static function List_data()
    {
        return Privileges::select('id','name','data')->where('status',1)->get();
    }

    public static function Single_data($privileges_id)
    {
        if(config('constant.CACHE')){
            $cache_name = "Single_data_".$privileges_id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Single_data_data($privileges_id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Single_data_data($privileges_id);
        }

        return $data;
    }

    public static function Single_data_data($privileges_id)
    {
        return Privileges::select('id','name','data')->where('status',1)->where('id',$privileges_id)->first();
    }
}
