<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Mongo_Test extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'majalah';

    protected $fillable = [
        'judul',
		'merk',
        'pengarang',
        'harga',
        'negara'
    ];

    protected $hidden = [
    ];
}
