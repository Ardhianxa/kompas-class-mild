<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityUsers extends Model
{
    protected $table = 'users_activity';
    protected $primaryKey = 'id';

    protected $fillable = [
        'activity_id',
		'user_id',
		'activity',
        'status'
    ];

    protected $hidden = [
    ];
}
