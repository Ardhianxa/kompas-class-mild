<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UsersInfo extends Model
{
    protected $table = 'users_info';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
		'nama_lengkap',
        'status'
    ];

    protected $hidden = [
    ];
}
