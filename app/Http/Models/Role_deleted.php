<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Role_deleted extends Model
{
    protected $table = 'role_deleted';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];
}
