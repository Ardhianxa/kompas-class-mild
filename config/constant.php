<?php
return [
    'MAIL_FROM' => env('MAIL_FROM'),
    'APP_NAME' => env('APP_NAME'),
    'ACTUAL_PATH' => env('ACTUAL_PATH'),
    'ASSETS_URL' => env('ASSETS_URL'),
    'LIMIT_DATA_TABLE' => env('LIMIT_DATA_TABLE'),
    'APP_URL' => env('APP_URL'),
    'FIREBASE_CHAT' => env('FIREBASE_CHAT'),
    'CACHE' => env('CACHE'),
    'CACHE_TIME' => env('CACHE_TIME_IN_MINUTES'),
    'MAX_SIZE_IMAGE_UPLOAD' => env('MAX_SIZE_IMAGE_UPLOAD'),
    'CONFIG_IMAGE_PROFILE' => json_encode(array(
        "config_name" => "config", 
        "file_manager_id_name" => "file_manager_id",
        "container" => "file-manager-data",
        "container_image" => "image_data",
        "multiple" => false,
        "validation" => [],
        "crop" => [],
        "auto_crop" => [
            "status" => true,
            "data" => [
                ["segment" => "public/assets/uploads/img/article/thumbnail", "width" => 319, "height" => 174],
                ["segment" => "public/assets/uploads/img/article/home", "width" => 154, "height" => 98]
            ]
        ],
        "editor" => false
    )),
    'CONFIG_IMAGE_ARTICLE_COVER' => json_encode(array(
        "config_name" => "config",
        "file_manager_id_name" => "file_manager_id",
        "container" => "file-manager-data",
        "container_image" => "image_data",
        "multiple" => false,
        "validation" => [
            "status" => false,
            "data" => [
                "orientation" => "landscape",
                "min_resolution" => [
                    "width" => 300,
                    "height" => 500
                ]
            ]
        ],
        "crop" => [
            "status" => false,
            "data" => [
                ["segment" => "thumbnail", "width" => 300, "height" => 250],
                ["segment" => "home", "width" => 300, "height" => 250]
              ]
        ],
        "auto_crop" => [
            "status" => true,
            "data" => [
                ["segment" => "public/assets/uploads/img/article/thumbnail", "width" => 300, "height" => 250],
                ["segment" => "public/assets/uploads/img/article/home", "width" => 300, "height" => 250]
            ]
        ],
        "editor" => false
    ))
];
