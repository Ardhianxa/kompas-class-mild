<?php
return [
    'modules'=>[
        'admin',
        'web',
        'api',
        'ConsumerLetter',
        'Inbox',
        'Channel',
        'Frontend',
        'Article',
        'ScreeningData'
    ]
];